#ifndef __PROGTEST__
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

//by:yehudnoa
//this code is for a tax registry. It takes in the names addresses and account names of people and stores there income and expenses
//no two people can have the same both name and address, or same account name.
//you can find people threw either there account name or there name and address and threw that you can change there income and expense or audit them

///this struct contains all the info of the person
struct database{
    string name, address, account;
    int income=0, expense=0;

    //constructor
    database(string nam, string addr, string acc){
        name = nam;
        address = addr;
        account = acc;
    }
};

///pointer to accounts to create array
struct ptr{
    database *act;
};

///class that sets and itorator and returns the values in the database struct
class CIterator         //itorator function
{
public:

    vector<ptr> ::iterator iterator;
    vector<ptr> tmp_vector;

    ///contructor
    CIterator( vector<ptr> b ){
        this->tmp_vector = b;
        iterator = tmp_vector.begin();
    }
    bool AtEnd ( void ) const{
        return iterator == tmp_vector.end();
    };
    void Next ( void ){
        ++iterator;
    };
    string Name ( void ) const{
        return iterator->act->name;
    };
    string Addr ( void ) const{
        return iterator->act->address;
    };
    string Account ( void ) const{
        return iterator->act->account;
    };

    ///distroctor
    ~CIterator(){
        tmp_vector.clear();
    };
private:

};

///in this class we control the array of database's
class CTaxRegister
{
public:
    int size;
    vector<ptr> AccountVec;      //array of pointers pointing to the structs in alphabetical order by account
    vector<ptr> NameVec;      //by name then address

    CTaxRegister(){             //constructor
        size = 0;
        AccountVec.reserve(100* sizeof(ptr));
        NameVec.reserve(100* sizeof(ptr));

    };

    ///adds database struct to both arrays if this combo of name and address doesn't exist and account name doesnt exist
    bool Birth ( const string & name, const string & addr, const string & account ){
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        ptr tmp2{};
        int mid;
        auto * person = new database (name, addr, account);         //creating struct
        tmp2.act = person;

        if((binary_search(NameVec.begin(), NameVec.end(), tmp2, compareByName) || (binary_search(AccountVec.begin(), AccountVec.end(), tmp2, compareByAccount))) ){ //looking if combo of name and address or if the account is in use
            delete person;
            return false;
        }

        mid = Find_Name_Array(tmp2);
        NameVec.insert(NameVec.begin()+mid, tmp2);           //putting struct in vector

        mid = Find_Account_Array(tmp2);
        AccountVec.insert(AccountVec.begin()+mid, tmp2);           //putting struct in vector

        size += 1;      //adding to size
        return true;
    };

    ///removes struct from both arrays
    bool Death ( const string & name, const string & addr ){// not finished so ignor
        int mid;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low2;
        database tmp(name, addr, name);
        ptr tmp2;
        tmp2.act = &tmp;

        if(binary_search(NameVec.begin(), NameVec.end(), tmp2, compareByName)){       //finding if person exists
            low = lower_bound(NameVec.begin(), NameVec.end(), tmp2, compareByName);
            mid = low - NameVec.begin();
            tmp2.act->account = NameVec[mid].act->account;
            low2 = lower_bound(AccountVec.begin(), AccountVec.end(), tmp2, compareByAccount);    //finds struct
            delete NameVec[mid].act;          //deleting struct
            AccountVec.erase(low2);
            NameVec.erase(low);               //erasing person

            size -= 1;      //minus the person
            return true;
        }

        return false;
    };

    ///find struct threw account name and add amount to the amount the person makes
    bool Income ( const string & account, int amount ){
        int mid;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        database tmp(account, account, account);
        ptr tmp2;
        tmp2.act = &tmp;

        if(binary_search(AccountVec.begin(), AccountVec.end(), tmp2, compareByAccount)){     //binary search to find account
            mid = Find_Account_Array(tmp2);
            AccountVec[mid].act->income += amount;       //adding the amount to amount
            return true;
        }

        return false;
    };

    ///find struct threw name and address of person and add amount to the amount the person makes
    bool Income ( const string & name, const string & addr, int amount ){
        int mid;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        database tmp(name, addr, name);
        ptr tmp2;
        tmp2.act = &tmp;

        if(binary_search(NameVec.begin(), NameVec.end(), tmp2, compareByName)){     //binary search to find account
            mid = Find_Name_Array(tmp2);
            NameVec[mid].act->income += amount;            //adding the amount to amount
            return true;
        }

        return false;
    };

    ///find struct threw account name and add amount to the expense the person makes
    bool Expense ( const string & account, int amount ){
        int mid;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        database tmp(account, account, account);
        ptr tmp2;
        tmp2.act = &tmp;

        if(binary_search(AccountVec.begin(), AccountVec.end(), tmp2, compareByAccount)){     //binary search to find account
            mid = Find_Account_Array(tmp2);
            AccountVec[mid].act->expense += amount;      //adding the amount to expense
            return true;
        }

        return false;
    };

    ///find struct threw name and address of person and add amount to the expense the person makes
    bool Expense ( const string & name, const string & addr, int amount ){
        int mid;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        database tmp(name, addr, name);
        ptr tmp2;
        tmp2.act = &tmp;

        if(binary_search(NameVec.begin(), NameVec.end(), tmp2, compareByName)){     //binary search to find account
            mid = Find_Name_Array(tmp2);
            NameVec[mid].act->expense += amount;       //adding the amount to expense
            return true;
        }

        return false;
    };

    ///finds struct threw name and address and returns the account name amount and expense
    bool Audit ( const string & name, const string & addr, string & account, int & sumIncome, int & sumExpense ){
        int mid;
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;
        database tmp(name, addr, name);
        ptr tmp2;
        tmp2.act = &tmp;

        if(binary_search(NameVec.begin(), NameVec.end(), tmp2, compareByName)){     //binary search to find account
            mid = Find_Name_Array(tmp2);
            account = NameVec[mid].act->account;           //returning everything to be checked
            sumIncome = NameVec[mid].act->income;
            sumExpense = NameVec[mid].act->expense;
            return true;
        }

        return false;
    };

    ///sends array that is listed by name to the citeraror class
    CIterator ListByName ( void ){
        CIterator b = NameVec;

        return b;
    };

    ///distructor
    ~CTaxRegister(){
        for(int i=0; i<size; i++){      //deletes all structs from memory
            delete AccountVec[i].act;
        }

        AccountVec.clear();
        NameVec.clear();
    };

private:

    ///compaires structs threw name and if they are equal by address
    static bool compareByName(const ptr &struct_a, const ptr &struct_b)       //compareing function for name then address
    {
        if(struct_a.act->name < struct_b.act->name){
            return true;
        }
        if((struct_a.act->name == struct_b.act->name) && (struct_a.act->address < struct_b.act->address)){
            return true;
        }
        else{
            return false;
        }
    };

    ///compaires structs threw accounts
    static bool compareByAccount(const ptr &struct_a, const ptr &struct_b)               //comparing function for account
    {
        return ((struct_a.act->account.compare(struct_b.act->account)) < 0);
    };

    ///returns either the place in the array B were the stuct should be inserted or were it currently is
    int Find_Account_Array(ptr tmp){
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;

        low = lower_bound(AccountVec.begin(), AccountVec.end(), tmp, compareByAccount);     //returns place in array that struct should be in array
        int mid = low - AccountVec.begin();

        return mid;
    };

    ///returns either the place in the array C were the stuct should be inserted or were it currently is
    int Find_Name_Array(ptr tmp){
        __gnu_cxx::__normal_iterator<ptr *, vector<ptr>> low;

        low = lower_bound(NameVec.begin(), NameVec.end(), tmp, compareByName);   //returns place in array that struct should be in array
        int mid = low - NameVec.begin();

        return mid;
    };

};


#ifndef __PROGTEST__
int main ( void )
{
    return 0;
}
#endif /* __PROGTEST__ */
