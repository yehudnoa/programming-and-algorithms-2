#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

string add(string a, string b){
    int j = a.size()-1;
    for(int i=b.size()-1; i>=0; i--, j--)
        a[j]+=(b[i]-'0');

    for(int i=a.size()-1; i>0; i--)
    {
        if(a[i] > '9')
        {
            int d = a[i]-'0';
            a[i-1] = ((a[i-1]-'0') + d/10) + '0';
            a[i] = (d%10)+'0';
        }
    }
    if(a[0] > '9')
    {
        string k;
        k+=a[0];
        a[0] = ((a[0]-'0')%10)+'0';
        k[0] = ((k[0]-'0')/10)+'0';
        a = k+a;
    }
    return a;
}

bool isSmaller(string str1, string str2)
{
    // Calculate lengths of both string
    int n1 = str1.length(), n2 = str2.length();

    if (n1 < n2)
        return true;
    if (n2 > n1)
        return false;

    for (int i=0; i<n1; i++)
    {
        if (str1[i] < str2[i])
            return true;
        else if (str1[i] > str2[i])
            return false;
    }
    return false;
}

// Function for finding difference of larger numbers
string findDiff(string str1, string str2)
{

    // Take an empty string for storing result
    string str = "";

    // Calculate lengths of both string
    int n1 = str1.length(), n2 = str2.length();
    int diff = n1 - n2;

    // Initially take carry zero
    int carry = 0;

    // Traverse from end of both strings
    for (int i=n2-1; i>=0; i--)
    {
        // Do school mathematics, compute difference of
        // current digits and carry
        int sub = ((str1[i+diff]-'0') -
                   (str2[i]-'0') -
                   carry);
        if (sub < 0)
        {
            sub = sub+10;
            carry = 1;
        }
        else
            carry = 0;

        str.push_back(sub + '0');
    }
    // subtract remaining digits of str1[]
    for (int i=n1-n2-1; i>=0; i--)
    {
        if (str1[i]=='0' && carry)
        {
            str.push_back('9');
            continue;
        }
        int sub = ((str1[i]-'0') - carry);
        if (i>0 || sub>0) // remove preceding 0's
            str.push_back(sub+'0');
        carry = 0;

    }
    // reverse resultant string
    reverse(str.begin(), str.end());

    return str;
}

string choose(string a, string b){
    string c;
    int i=0;
    if(a[0] == '-'){
        a.erase(a.begin());
        i+=1;
    }    if(b[0] == '-'){
        b.erase(b.begin());
        i+=2;
    }

    if( i == 0 || i == 3){
        if(a.size() < b.size())
            swap(a, b);
        c = add(a, b);
        if(i == 3){
            c.insert(c.begin(), '-');
        }
    }
    if( (i == 1 && a > b ) || (i == 2 && a < b )){
        if (isSmaller(a, b)){
            swap(a, b);
        }
        c = findDiff( a , b);
        c.insert(c.begin(), '-');
    }
    if( (i == 1 && a < b ) || (i == 2 && a < b )){
        if (isSmaller(a, b)){
            swap(a, b);
        }
        c = findDiff( a , b);
    }
    return c;
}

string int_to_string(int a){
    stringstream ss;
    ss << a;
    string str = ss.str();
    return str;
}
string multiply(string num1, string num2) {
    if(num1.size()==0||num2.size()==0) return "";
    string ret(num1.size()+num2.size()+1,'0');
    reverse(num1.begin(),num1.end());
    reverse(num2.begin(),num2.end());
    int size1 = num2.size();
    int size = num1.size();
    for(int i=0;i<size1;i++){
        int dig2=num2[i]-'0';
        int carry=0;
        for(int j=0;j<size;j++){
            int dig1=num1[j]-'0';
            int temp=ret[i+j]-'0';
            int cur=dig1*dig2+temp+carry;
            ret[i+j]=cur%10+'0';
            carry=cur/10;
        }
        ret[i+num1.size()]=carry+'0';
    }
    reverse(ret.begin(),ret.end());
    int pos=ret.find_first_not_of('0',0);
    int retsize = ret.size();
    if(pos<0||pos>=retsize)
        pos=ret.size()-1;
    return ret.substr(pos,ret.size()-pos);
}

bool check(string & a){
    string tmp;
    int size = a.size();
    for(int i = 0;i < size;i++) {
        while((a[i]=='0' && i == 0) || (a[i] == '0' && i == 1 && a[0] == '-')){
            a.erase(a.begin() + i);
        }
        if( i == 0 && a[0] == '-') {
            tmp.push_back(a[i]);
        }
        else if(a[i] != ' ' && a[i] != '1' && a[i] != '2' && a[i] != '3' && a[i] != '4' && a[i] != '5' && a[i] != '6' && a[i] != '7' && a[i] != '8' && a[i] != '9' && a[i] != '0') {
            break;
        }
        else if(((i != 0) || (a[0] == '-' && i != 1)) && (a[i] == ' ')) {
            break;
        }else{
            tmp.push_back(a[i]);
        }
    }
    if(tmp.size() < 3){
        if( (tmp.size() == 1 && tmp[0] == '-') || (tmp.size() == 0) ){
            return true;
        }
    }
    a = tmp;
    return false;
}


string mult(string a, string b){
    int i = 0;
    if(a[0] == '-'){
        i++;
        a.erase(a.begin());
    }
    if(b[0] == '-'){
        i++;
        b.erase(b.begin());
    }
    string c = multiply(a,b);
    if(i == 1 && c[0] != '0'){
        c.insert(c.begin(), '-');
    }
    return c;
}

int comp(string a, string b){
    int i=0;
    while(a[i] == '0'){
        a.erase(a.begin());
    }while(a[0] == '-' && a[1] == '0'){
        a.erase(a.begin() + 1 );
    }
    if(b[0] == '0'){
        b.erase(b.begin());
    }if(b[0] == '-' && b[1] == '0'){
        b.erase(b.begin() + 1 );
    }
    if(a[0] == '-'){
        a.erase(a.begin());
        i+=1;
    }    if(b[0] == '-'){
        b.erase(b.begin());
        i+=2;
    }
    if(i == 1){
        return -1;
    }
    if(i == 2){
        return 1;
    }
    if(a.size() < b.size()){
        if(i == 3){
            return 1;
        }
        return -1;
    }
    if(a.size() > b.size()){
        if(i == 3){
            return -1;
        }
        return 1;
    }
    int size= a.size();
    for(int i = 0; i < size; i++){
        if(a[i] < b[i]){
            if(i == 3){
                return 1;
            }
            return -1;
        }
        if(a[i] > b[i]){
            if(i == 3){
                return -1;
            }
            return 1;
        }
    }
    return 0;
}

class CBigInt
{
public:
    string num;
    // default constructor
    CBigInt(){
        num = '0';
    }
    // copying/assignment/destruction
    // int constructor
    CBigInt(int in){
        stringstream ss;
        ss << in;
        num = ss.str();
    }
    // string constructor
    CBigInt(const char* in){
        string tmp = in;
        if(check(tmp)){
            throw invalid_argument("ERROR");
        }else{
            num = in;
        }
    }
    // operator +, any combination {CBigInt/int/string} + {CBigInt/int/string}
    friend CBigInt operator +(CBigInt a, CBigInt b){
        string c = choose(a.num, b.num);
        CBigInt d;
        d.num = c;
        return d;
    }
    friend CBigInt operator +(CBigInt a, int b){
        string str = int_to_string(b);
        string c = choose(a.num, str);
        CBigInt d;
        d.num = c;
        return d;
    }
    friend CBigInt operator +(CBigInt a, const char* tmp){
        string b = tmp;
        if(check(b)){
            throw invalid_argument("ERROR");
        }
        string c = choose(a.num, b);
        CBigInt d;
        d.num = c;
        return d;
    }
    friend CBigInt operator +(int a, CBigInt b){
        CBigInt c;
        c = b + a;
        return c;
    }
    friend CBigInt operator +(const char* tmp, CBigInt b){
        string a = tmp;
        if(check(a)){
            throw invalid_argument("ERROR");
        }
        string c = choose(a, b.num);
        CBigInt d;
        d.num = c;
        return d;
    }
    // operator *, any combination {CBigInt/int/string} * {CBigInt/int/string}
    friend CBigInt operator *(CBigInt a, CBigInt b){
        CBigInt c;
        c.num = mult(a.num, b.num);
        return c;
    }
    friend CBigInt operator *(CBigInt a, int b){
        CBigInt c;
        string tmp = int_to_string(b);
        c.num = mult(a.num, tmp);
        return c;
    }
    friend CBigInt operator *(CBigInt a, const char* tmp){
        string b = tmp;
        if(check(b)){
            throw invalid_argument("ERROR");
        }
        CBigInt c;
        c.num = mult(a.num, b);
        return c;
    }
    friend CBigInt operator *(int a, CBigInt b){
        CBigInt c;
        c = b * a;
        return c;
    }
    friend CBigInt operator *(const char* tmp, CBigInt b){
        string a = tmp;
        if(check(a)){
            throw invalid_argument("ERROR");
        }
        CBigInt c;
        c.num = mult(a, b.num);
        return c;
    }
    // operator +=, any of {CBigInt/int/string}
    friend void operator +=(CBigInt & a, CBigInt b){
        string c = choose(a.num, b.num);
        a.num = c;
    }
    friend void operator +=(CBigInt & a, int b){
        string str = int_to_string(b);
        string c = choose(a.num, str);
        a.num = c;
    }
    friend void operator +=(CBigInt & a, const char* tmp){
        string b = tmp;
        if(check(b)){
            throw invalid_argument("ERROR");
        }
        string c = choose(a.num, b);
        a.num = c;
    }
    // operator *=, any of {CBigInt/int/string}
    friend void operator *=(CBigInt & a, CBigInt b){
        CBigInt c;
        a.num = mult(a.num, b.num);
    }
    friend void operator *=(CBigInt & a, int b){
        CBigInt c;
        string tmp = int_to_string(b);
        a.num = mult(a.num, tmp);
    }
    friend void operator *=(CBigInt & a, const char* tmp){
        string b = tmp;
        if(check(b)){
            throw invalid_argument("ERROR");
        }
        CBigInt c;
        a.num = mult(a.num, b);
    }
    // comparison operators, any combination {CBigInt/int/string} {<,<=,>,>=,==,!=} {CBigInt/int/string}
    friend bool operator <=(CBigInt a, CBigInt b){
        int i = comp(a.num, b.num);
        return ( i == -1 || i == 0);
    }
    friend bool operator <=(CBigInt a, int b){
        string str = int_to_string(b);
        int i = comp(a.num, str);
        return ( i == -1 || i == 0);
    }
    friend bool operator <=(CBigInt a, const char* tmp){
        string b = tmp;
        int i = comp(a.num, b);
        return ( i == -1 || i == 0);
    }
    friend bool operator >=(CBigInt a, CBigInt b){
        int i = comp(a.num, b.num);
        return ( i == 1 || i == 0);
    }
    friend bool operator >=(CBigInt a, int b){
        string str = int_to_string(b);
        int i = comp(a.num, str);
        return ( i == 1 || i == 0);
    }
    friend bool operator >=(CBigInt a, const char* tmp){
        string b = tmp;
        int i = comp(a.num, b);
        return ( i == 1 || i == 0);
    }
    friend bool operator ==(CBigInt a, CBigInt b){
        return (a.num.compare(b.num)==0);
    }
    friend bool operator ==(CBigInt a, int b){
        string str = int_to_string(b);
        return (a.num.compare(str)==0);
    }
    friend bool operator ==(CBigInt a, const char* tmp){
        string b = tmp;
        if(check(b)){
            return false;
        }
        return (a.num.compare(b)==0);
    }
    friend bool operator !=(CBigInt a, CBigInt b){
        return (a.num.compare(b.num)!=0);
    }
    friend bool operator !=(CBigInt a, int b){
        string str = int_to_string(b);
        return (a.num.compare(str)!=0);
    }
    friend bool operator !=(CBigInt a, const char* tmp){
        string b = tmp;
        return (a.num.compare(b)!=0);
    }
    friend bool operator <(CBigInt a, CBigInt b){
        int i = comp(a.num, b.num);
        return ( i == -1 );
    }
    friend bool operator <(CBigInt a, int b){
        string str = int_to_string(b);
        int i = comp(a.num, str);
        return ( i == -1 );
    }
    friend bool operator <(CBigInt a, const char* tmp){
        int i = comp(a.num, tmp);
        return ( i == -1 );
    }
    friend bool operator >(CBigInt a, CBigInt b){
        int i = comp(a.num, b.num);
        return ( i == 1 );
    }
    friend bool operator >(CBigInt a, int b){
        string str = int_to_string(b);
        int i = comp(a.num, str);
        return ( i == 1 );
    }
    friend bool operator >(CBigInt a, const char* tmp){
        int i = comp(a.num, tmp);
        return ( i == 1 );
    }
    // output operator <<
    friend void operator <<(ostream & a, CBigInt b){
        a << b.num;
    }
    // input operator >>
    friend bool operator >>(istream & a, CBigInt & b){
        string tmp;
        a >> tmp;
        if(check(tmp)){
            return false;
        }
        b.num = tmp;
        return true;
    }
private:
    // todo
};

#ifndef __PROGTEST__
static bool equal ( const CBigInt & x, const char * val )
{
    ostringstream oss;
    oss << x;
    return oss . str () == val;
}
int main ( void )
{
    CBigInt a, b;
    istringstream is;
    a = 10;
    a += 20;
    assert ( equal ( a, "30" ) );
    a *= 5;
    assert ( equal ( a, "150" ) );
    b = a + 3;
    assert ( equal ( b, "153" ) );
    b = a * 7;
    assert ( equal ( b, "1050" ) );
    assert ( equal ( a, "150" ) );
    a = 10;
    a += -20;
    assert ( equal ( a, "-10" ) );
    a *= 5;
    assert ( equal ( a, "-50" ) );
    b = a + 73;
    assert ( equal ( b, "23" ) );
    b = a * -7;
    assert ( equal ( b, "350" ) );
    assert ( equal ( a, "-50" ) );

    a = "12345678901234567890";
    a += "-99999999999999999999";
    assert ( equal ( a, "-87654321098765432109" ) );
    a *= "54321987654321987654";
    assert ( equal ( a, "-4761556948575111126880627366067073182286" ) );
    a *= 0;
    assert ( equal ( a, "0" ) );
    a = 10;
    b = a + "400";
    assert ( equal ( b, "410" ) );
    b = a * "15";
    assert ( equal ( b, "150" ) );
    assert ( equal ( a, "10" ) );

    a = "763348867813483499878769948804870115991783031370358135139910497518008277680817843767270261201062863790458430225691429445676872069348494247360939072927594272579413353787414407415717476993241192527896537988730201058747162281533322986775559680795569386615769191499167732245302185447478989732998242796932563691215873765563761659614313193282260760800427334526";
    a+="0763348867813483499878769948804870115991783031370358135139910497518008277680817843767270261201062863790458430225691429445676872069348494247360939072927594272579413353787414407415717476993241192527896537988730201058747162281533322986775559680795569386615769191499167732245302185447478989732998242796932563691215873765563761659614313193282260760800427334526";
    assert(a =="1526697735626966999757539897609740231983566062740716270279820995036016555361635687534540522402125727580916860451382858891353744138696988494721878145855188545158826707574828814831434953986482385055793075977460402117494324563066645973551119361591138773231538382998335464490604370894957979465996485593865127382431747531127523319228626386564521521600854669052");

    a = "05";
    a *= 5;
    assert(a == 25);

    is . clear ();
    is . str ( " 1234" );
    assert ( is >> b );
    assert ( equal ( b, "1234" ) );
    is . clear ();
    is . str ( " 12 34" );
    assert ( is >> b );
    assert ( equal ( b, "12" ) );
    is . clear ();
    is . str ( "999z" );
    assert ( is >> b );
    assert ( equal ( b, "999" ) );
    is . clear ();
    is . str ( "abcd" );
    assert ( ! ( is >> b ) );
    is . clear ();
    is . str ( "- 758" );
    assert ( ! ( is >> b ) );
    a = 42;
    try
    {
        a = "-xyz";
        assert ( "missing an exception" == NULL );
    }
    catch ( const invalid_argument & e )
    {
        assert ( equal ( a, "42" ) );
    }

    a = "73786976294838206464";
    assert ( a < "1361129467683753853853498429727072845824" );
    assert ( a <= "1361129467683753853853498429727072845824" );
    assert ( ! ( a > "1361129467683753853853498429727072845824" ) );
    assert ( ! ( a >= "1361129467683753853853498429727072845824" ) );
    assert ( ! ( a == "1361129467683753853853498429727072845824" ) );
    assert ( a != "1361129467683753853853498429727072845824" );
    assert ( ! ( a < "73786976294838206464" ) );
    assert ( a <= "73786976294838206464" );
    assert ( ! ( a > "73786976294838206464" ) );
    assert ( a >= "73786976294838206464" );
    assert ( a == "73786976294838206464" );
    assert ( ! ( a != "73786976294838206464" ) );
    assert ( a < "73786976294838206465" );
    assert ( a <= "73786976294838206465" );
    assert ( ! ( a > "73786976294838206465" ) );
    assert ( ! ( a >= "73786976294838206465" ) );
    assert ( ! ( a == "73786976294838206465" ) );
    assert ( a != "73786976294838206465" );
    a = "2147483648";
    assert ( ! ( a < -2147483648 ) );
    assert ( ! ( a <= -2147483648 ) );
    assert ( a > -2147483648 );
    assert ( a >= -2147483648 );
    assert ( ! ( a == -2147483648 ) );
    assert ( a != -2147483648 );
    return 0;
}
#endif /* __PROGTEST__ */