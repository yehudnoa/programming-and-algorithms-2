#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;
#endif /* __PROGTEST__ */


///struct for transactions
struct trans {
    trans *next = NULL;
    trans *prev = NULL;
    char *sign;
    char *from;
    char *to;
    char c = {'\0'};
    int amount = 0;
    int bal;
    trans(){

    }
    trans(const char * Tsign, const char* Tfrom, const char* Tto, char Tc, int Tamount, int Tbal){
        sign = strdup(Tsign);
        from = strdup(Tfrom);
        to = strdup(Tto);
        c = Tc;
        amount = Tamount;
        bal = Tbal;
    }
};

///struct that holds account info
struct acc{
    acc *next = nullptr;
    acc *prev = nullptr;
    char *id;
    int balence;
    struct trans *a = nullptr;      //points to transaction listZq
    acc(){

    }
    acc(const char * accID, int accBal){
        id = strdup(accID);
        balence = accBal;
    }
    int Balance() {
        return balence;
    }

};

///if shallow copy will show how many lists are pointing to the same list
struct Shallow{
    int cnt = 0;
};


class CBank {
public:
    struct acc *data;
    struct acc *begin;
    struct acc *end;
    Shallow *cpy = NULL;             //if shallow copy will show here

    /// default constructor
    CBank() {
        data = nullptr;
        begin = nullptr;
        end = nullptr;
    }

    /// copy constructor
    CBank(CBank &a) {
        if( a.data == nullptr){     // if copying null pointer make equal to null
            data = nullptr;
            begin = nullptr;
            end = nullptr;
        }else{
            if(a.cpy== nullptr){    //if a is not already a shallow copy
                auto * shalcpy = new Shallow;       //makes shallow copy and connects them in group
                cpy = shalcpy;
                cpy->cnt += 1;
                a.cpy = shalcpy;
                a.cpy->cnt += 1;

            }else{
                cpy = a.cpy;        //if already shallow copy add to group
                cpy->cnt += 1;
            }
            begin = a.begin;
            end = a.end;
            data = a.data;
        }
    }

    ///adds transaction list to accounts
    trans *addT(trans *a) {
        while (a->prev != nullptr) {    //makes sure its at beginning
            a = a->prev;
        }
        struct trans *curr = a, *tmp;

        while (curr) {          //same as deep copy
            tmp = curr->next;
            curr->next = new trans(curr->sign, curr->from, curr->to, curr->c, curr->amount, curr->bal);
            curr->next->next = tmp;
            curr = tmp;
        }
        curr = a;
        struct trans *original = a, *copy = a->next;
        tmp = copy;
        while (copy->next) {
            original->next = original->next->next;
            copy->next = copy->next->next;

            original = original->next;
            copy = copy->next;
        }
        original->next = NULL;
        copy = tmp;
        while (copy->next) {
            copy->next->prev = copy;
            copy = copy->next;
        }
        original = a;
        while (original->next) {
            original->next->prev = original;
            original = original->next;
        }
        return copy;
    }

    /// destructor
    ~CBank() {
        if(cpy != nullptr){
            if(cpy->cnt > 1){
                cpy->cnt -= 1;
                begin = end = data = nullptr;
            }else{
                delete cpy;
                cpy = nullptr;
            }
        }
        if (begin != nullptr) {
            acc *iterator = begin;

            while (iterator != nullptr) {
                begin = iterator->next;
                if (iterator->a != nullptr) {
                    trans *iteratorT = iterator->a;
                    while (iteratorT->prev) {
                        iteratorT = iteratorT->prev;
                    }
                    data = iterator;
                    while (iteratorT != nullptr) {
                        data->a = iteratorT->next;
                        free(iteratorT->sign);
                        free(iteratorT->from);
                        free(iteratorT->to);
                        delete iteratorT;
                        iteratorT = data->a;
                    }

                    delete data->a;
                }

                free(iterator->id);
                delete iterator;
                iterator = begin;
            }

            delete begin;
        }
    }
    // operator =

    ///adding new account
    bool NewAccount(const char *accID, int initialBalance) {
        if(cpy != NULL){                // checking if this is shallow copy and if only version
            if(cpy->cnt > 1){           //if more versions make deep copy;
                cpy->cnt -= 1;
                cpy = nullptr;
                DeepCopy(this);
            }else{                      //if only version keep as deep copy
                delete cpy;
                cpy = nullptr;
            }
        }
        struct acc *tmp2;
        auto *tmp = new struct acc(accID, initialBalance);
        if (data == nullptr) {                  //if list is empty put in
            data = tmp;
            begin = tmp;
            end = tmp;
            return true;
        }
        while (begin->prev != nullptr) {       //check at beginning
            begin = begin->prev;
        }
        tmp2 = begin;
        tmp2 = search(accID, tmp2);             //checking if account already exists
        if (tmp2 != nullptr) {
            free(tmp->id);
            delete tmp;
            return false;
        } else {
            end->next = tmp;
            tmp->prev = end;
            end = end->next;
        };
        return true;
    };

    ///adds trans action to list
    bool Transaction(const char *debAccID, const char *credAccID, unsigned int amount, const char *signature) {
        if(cpy != NULL){                // checking if this is shallow copy and if only version
            if(cpy->cnt > 1){           //if more versions make deep copy;
                cpy->cnt -= 1;
                cpy = nullptr;
                DeepCopy(this);
            }else{                      //if only version keep as deep copy
                delete cpy;
                cpy = nullptr;
            }
        }
        if (strcmp(debAccID, credAccID) == 0) {        //if to and from are the same ERROR
            return false;
        }

        if (data == nullptr) {                          //if no accounts in list
            return false;
        }
        struct acc *tmp, *tmp2;
        while (begin->prev != nullptr) {                //making sure at begining
            begin = begin->prev;
        }
        tmp = begin;
        tmp = search(debAccID, tmp);                    //finding from
        if (tmp == nullptr) {
            return false;
        };
        while (begin->prev != nullptr) {                //making sure its at the begining
            begin = begin->prev;
        }
        tmp2 = begin;
        tmp2 = search(credAccID, tmp2);                 //finding to
        if (tmp2 == nullptr) {
            return false;
        };

        struct trans *T3;
        auto *T = new trans(signature, debAccID, credAccID, '-', amount, tmp->balence);
        auto *T1 = new trans(signature, debAccID, credAccID, '+', amount, tmp2->balence);
        tmp->balence -= amount;
        tmp2->balence += amount;
        if (tmp->a == nullptr) {            //adding transaction to from
            tmp->a = T;
        } else {
            T3 = tmp->a;
            while (T3->next != nullptr) {
                T3 = T3->next;
            }
            T->prev = T3;
            T3->next = T;
        }
        if (tmp2->a == nullptr) {           //adding transactions to to
            tmp2->a = T1;
        } else {
            T3 = tmp2->a;
            while (T3->next != nullptr) {
                T3 = T3->next;
            }
            T1->prev = T3;
            T3->next = T1;
        }
        return true;
    };

    ///erases tansactions in the account
    bool TrimAccount(const char *accID) {
        if(cpy != NULL){            //if shallow copy and not the only version make deep copy
            if(cpy->cnt > 1){
                cpy->cnt -= 1;
                cpy = nullptr;
                DeepCopy(this);
            }else{                  //if only version of this keep this as only version
                delete cpy;
                cpy = nullptr;
            }
        }
        struct acc *tmp;
        tmp = begin;
        tmp = search(accID, tmp);       //finds struct
        if (tmp == nullptr) {
            return false;
        }
        if(tmp->a->prev != nullptr){
            while(tmp->a->prev != nullptr){
                tmp->a =tmp->a->prev;
            }
        }
        struct trans *tmp1, *tmp2;
        tmp2 = tmp1 = tmp->a;
        while (tmp1->next != nullptr) {  // deletes transactions
            tmp1 = tmp1->next;
            free(tmp2->sign);
            free(tmp2->from);
            free(tmp2->to);
            delete tmp2;
            tmp2 = tmp1;
        }
        free(tmp1->sign);
        free(tmp1->from);
        free(tmp1->to);
        delete tmp1;
        tmp->a = nullptr;
        return true;
    };

    ///makes a deep copy of link list with the transactions
    void DeepCopy(CBank *a){
        struct acc *curr = a->begin, *tmp;
        while(curr){                                 //makes origstruct -> copy struct -> origstruct -> copystruct
            tmp = curr->next;
            curr->next = new acc(curr->id, curr->balence);
            if (curr->a != nullptr)
                curr->next->a = addT(curr->a);      //adds transaction to account see addT
            curr->next->next = tmp;
            curr = tmp;
        }
        curr = a->begin;
        struct acc *original = a->begin, *copy = a->begin->next; //starting to split list into 2 lists
        tmp = copy;

        while(copy->next){
            original->next = original->next->next;
            copy->next = copy->next->next;
            original = original->next;
            copy = copy->next;
        }
        original->next = NULL;
        copy = tmp;
        while(copy->next){              //adding the prev pointers to copy
            copy->next->prev = copy;
            copy = copy->next;
        }
        original = a->begin;
        while(original->next){                  //adding the prev pointers to original
            original->next->prev = original;
            original = original->next;
        }
        data = tmp;
        begin = data;
        end = data;
        while(end->next){
            end = end->next;
        }
    }

    // Account ( accID )
    ///returns struct of acc
    struct acc Account(const char * accID){
        struct acc *tmp;
        if(data == nullptr){                    //if in empty list throw error
            throw invalid_argument("ERROR");
        }
        while(begin->prev != nullptr){          //making sure iam at begining of list
            begin = begin->prev;
        }
        tmp = begin;
        tmp = search(accID, tmp);               //searching for the account name
        if(tmp != NULL){                        //if account exists return account if not throw error
            return *tmp;
        }
        throw invalid_argument("ERROR");
    }

    void friend operator <<(ostream & a, CBank b);

    /// equals operator
    /// first checks that that list that being copied into is empty
    void operator=(CBank &b){
        if(b.data != nullptr){          //making sure pointer copying from is not empty
            if(data != nullptr && (cpy == nullptr || (cpy != nullptr && cpy->cnt == 1))){        //making sure pointer copying to is empty
                                        //when it isnt here deletes list
                if(cpy != nullptr && cpy->cnt == 1){
                    delete cpy;
                    cpy = nullptr;
                }
                acc* iterator = begin;

                while (iterator != nullptr)
                {
                    begin = iterator->next;
                    if(iterator->a != nullptr){
                        trans* iteratorT = iterator->a;
                        while(iteratorT->prev){
                            iteratorT = iteratorT->prev;
                        }
                        data = iterator;
                        while (iteratorT != nullptr)
                        {
                            data->a = iteratorT->next;
                            free(iteratorT->sign);
                            free(iteratorT->from);
                            free(iteratorT->to);
                            delete iteratorT;
                            iteratorT = data->a;
                        }

                        delete data->a;
                    }

                    free(iterator->id) ;
                    delete iterator;
                    iterator = begin;
                }

                delete begin;

            }else if(cpy != nullptr){
                cpy->cnt -= 1;
                cpy = nullptr;
                begin = nullptr;
                end = nullptr;
                data = nullptr;
            }
            if(b.cpy== nullptr){        //if Cbank B is not a shallow copy too
                auto * shalcpy = new Shallow;
                cpy = shalcpy;
                cpy->cnt += 1;
                b.cpy = shalcpy;
                b.cpy->cnt += 1;

            }else{                      //if b is also a shallow copy we add this new Cbank to shallow ptr group
                cpy = b.cpy;
                cpy->cnt += 1;
            }
            begin = b.begin;
            end = b.end;
            data = b.data;
        }
    }

private:

    ///searches for account in the list and returns the struct
    struct acc * search(const char * id, struct acc *tmp){
        if(strcmp(tmp->id, id)==0){
            return tmp;
        }
        while(tmp->next != nullptr){
            if(strcmp(tmp->id, id)==0){
                return tmp;
            }else{
                tmp = tmp->next;
            }
        }
        if(strcmp(tmp->id, id)==0){
            return tmp;
        }else{
            return NULL;
        }
    }
};

///prints out all the transactions
void operator<<(ostream &a, struct acc b) {
    if(b.a != nullptr && b.a->next != nullptr){
        while(b.a->prev){
            b.a = b.a->prev;
        }
    }
    trans * tmp = b.a;
    if(b.a == nullptr){
        a << b.id << ":\n   " << b.balence << endl ;
    }else{
        a << b.id << ":\n   " << tmp->bal << endl ;
        while(tmp->next != nullptr){
            a << " " << tmp->c << " " << tmp->amount << ", ";
            if(tmp->c == '-'){
                a << "to: " << tmp->to << ", sign: " << tmp->sign << endl;
            }else{
                a << "from: " << tmp->from << ", sign: " << tmp->sign << endl;
            }
            tmp = tmp->next;
        }
        a <<" " << tmp->c << " " << tmp->amount << ", ";
        if(tmp->c == '-'){
            a << "to: " << tmp->to << ", sign: " << tmp->sign << endl;
        }else{
            a << "from: " << tmp->from << ", sign: " << tmp->sign << endl;
        }
    }
    a << " = " << b.balence << endl;
}

#ifndef __PROGTEST__
int main ( void )
{
    ostringstream os;
    char accCpy[100], debCpy[100], credCpy[100], signCpy[100];
    CBank x0;
    CBank x1(x0);
    assert ( x0 . NewAccount ( "123456", 1000 ) );
    assert ( x0 . NewAccount ( "987654", -500 ) );
    assert ( x0 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x0 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x0 . NewAccount ( "111111", 5000 ) );
    assert ( x0 . Transaction ( "111111", "987654", 290, "Okh6e+8rAiuT5=" ) );
    assert ( x0 . Account ( "123456" ). Balance ( ) ==  -2190 );
    assert ( x0 . Account ( "987654" ). Balance ( ) ==  2980 );
    assert ( x0 . Account ( "111111" ). Balance ( ) ==  4710 );
    os . str ( "" );
    os << x0 . Account ( "123456" );
    assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
    os . str ( "" );
    os << x0 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 290, from: 111111, sign: Okh6e+8rAiuT5=\n = 2980\n" ) );
    os . str ( "" );
    os << x0 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 290, to: 987654, sign: Okh6e+8rAiuT5=\n = 4710\n" ) );

    CBank x2;
    strncpy ( accCpy, "123456", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, 1000 ));
    strncpy ( accCpy, "987654", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, -500 ));
    strncpy ( debCpy, "123456", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "XAbG5uKz6E=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 300, signCpy ) );
    strncpy ( debCpy, "123456", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "AbG5uKz6E=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
    strncpy ( accCpy, "111111", sizeof ( accCpy ) );
    assert ( x2 . NewAccount ( accCpy, 5000 ));
    strncpy ( debCpy, "111111", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "Okh6e+8rAiuT5=", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 2890, signCpy ) );
    assert ( x2 . Account ( "123456" ). Balance ( ) ==  -2190 );
    assert ( x2 . Account ( "987654" ). Balance ( ) ==  5580 );
    assert ( x2 . Account ( "111111" ). Balance ( ) ==  2110 );
    os . str ( "" );
    os << x2 . Account ( "123456" );
    assert ( ! strcmp ( os . str () . c_str (), "123456:\n   1000\n - 300, to: 987654, sign: XAbG5uKz6E=\n - 2890, to: 987654, sign: AbG5uKz6E=\n = -2190\n" ) );
    os . str ( "" );
    os << x2 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n = 5580\n" ) );
    os . str ( "" );
    os << x2 . Account ( "111111" );
    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n = 2110\n" ) );
    assert ( x2 . TrimAccount ( "987654" ) );
    strncpy ( debCpy, "111111", sizeof ( debCpy ) );
    strncpy ( credCpy, "987654", sizeof ( credCpy ) );
    strncpy ( signCpy, "asdf78wrnASDT3W", sizeof ( signCpy ) );
    assert ( x2 . Transaction ( debCpy, credCpy, 123, signCpy ) );
    os . str ( "" );
    os << x2 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   5580\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );

    CBank x4;
    assert ( x4 . NewAccount ( "123456", 1000 ) );
    assert ( x4 . NewAccount ( "987654", -500 ) );
    assert ( !x4 . NewAccount ( "123456", 3000 ) );
    assert ( !x4 . Transaction ( "123456", "666", 100, "123nr6dfqkwbv5" ) );
    assert ( !x4 . Transaction ( "666", "123456", 100, "34dGD74JsdfKGH" ) );
    assert ( !x4 . Transaction ( "123456", "123456", 100, "Juaw7Jasdkjb5" ) );
    try
    {
        x4 . Account ( "666" ). Balance ( );
        assert ( "Missing exception !!" == NULL );
    }
    catch ( ... )
    {
    }
    try
    {
        os << x4 . Account ( "666" ). Balance ( );
        assert ( "Missing exception !!" == NULL );
    }
    catch ( ... )
    {
    }
    assert ( !x4 . TrimAccount ( "666" ) );

    CBank x6;
    assert ( x6 . NewAccount ( "123456", 1000 ) );
    assert ( x6 . NewAccount ( "987654", -500 ) );
    assert ( x6 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
    assert ( x6 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
    assert ( x6 . NewAccount ( "111111", 5000 ) );
    assert ( x6 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
    CBank x7 ( x6 );
    assert ( x6 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
    assert ( x7 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
    assert ( x6 . NewAccount ( "99999999", 7000 ) );
    assert ( x6 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
    assert ( x6 . TrimAccount ( "111111" ) );
    assert ( x6 . Transaction ( "123456", "111111", 221, "Q23wr234ER==" ) );
    os . str ( "" );
    os << x6 . Account ( "111111" );

    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n + 221, from: 123456, sign: Q23wr234ER==\n = -1581\n" ) );
    os . str ( "" );
    os << x6 . Account ( "99999999" );
    assert ( ! strcmp ( os . str () . c_str (), "99999999:\n   7000\n + 3789, from: 111111, sign: aher5asdVsAD\n = 10789\n" ) );
    os . str ( "" );
    os << x6 . Account ( "987654" );

    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 123, from: 111111, sign: asdf78wrnASDT3W\n = 5703\n" ) );
    os . str ( "" );
    os << x7 . Account ( "111111" );

    assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );
    try
    {
        os << x7 . Account ( "99999999" ). Balance ( );
        assert ( "Missing exception !!" == NULL );
    }
    catch ( ... )
    {
    }
    os . str ( "" );
    os << x7 . Account ( "987654" );
    assert ( ! strcmp ( os . str () . c_str (), "987654:\n   -500\n + 300, from: 123456, sign: XAbG5uKz6E=\n + 2890, from: 123456, sign: AbG5uKz6E=\n + 2890, from: 111111, sign: Okh6e+8rAiuT5=\n + 789, from: 111111, sign: SGDFTYE3sdfsd3W\n = 6369\n" ) );

       CBank x8;
       CBank x9;
       CBank x10;
       assert ( x8 . NewAccount ( "123456", 1000 ) );
       assert ( x8 . NewAccount ( "987654", -500 ) );
        assert ( x8 . NewAccount ( "1", 1000 ) );
        assert ( x8 . NewAccount ( "2", -500 ) );
    assert ( x8 . NewAccount ( "3", 1000 ) );
    assert ( x8 . NewAccount ( "4", -500 ) );
    assert ( x8 . NewAccount ( "5", 1000 ) );
    assert ( x8 . NewAccount ( "6", -500 ) );
    assert ( x8 . NewAccount ( "7", 1000 ) );
    assert ( x8 . NewAccount ( "8", -500 ) );
       assert ( x8 . Transaction ( "123456", "987654", 300, "XAbG5uKz6E=" ) );
       assert ( x8 . Transaction ( "123456", "987654", 2890, "AbG5uKz6E=" ) );
       assert ( x8 . NewAccount ( "111111", 5000 ) );
       assert ( x8 . Transaction ( "111111", "987654", 2890, "Okh6e+8rAiuT5=" ) );
       x9 = x8;
       x8 = x9;
       x10 = x9;
       x0 = x8;
       x10.TrimAccount("123456");
       assert ( x8 . Transaction ( "111111", "987654", 123, "asdf78wrnASDT3W" ) );
       assert ( x9 . Transaction ( "111111", "987654", 789, "SGDFTYE3sdfsd3W" ) );
       assert ( x8 . NewAccount ( "99999999", 7000 ) );
       assert ( x8 . Transaction ( "111111", "99999999", 3789, "aher5asdVsAD" ) );
       assert ( x8 . TrimAccount ( "111111" ) );
       os . str ( "" );
       os << x8 . Account ( "111111" );
       assert ( ! strcmp ( os . str () . c_str (), "111111:\n   -1802\n = -1802\n" ) );
       os . str ( "" );
       os << x9 . Account ( "111111" );
       assert ( ! strcmp ( os . str () . c_str (), "111111:\n   5000\n - 2890, to: 987654, sign: Okh6e+8rAiuT5=\n - 789, to: 987654, sign: SGDFTYE3sdfsd3W\n = 1321\n" ) );

        return 0;
}
#endif /* __PROGTEST__ */


