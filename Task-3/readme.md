Your task is to implement class CBank, which implements an internet banking system.

The class is essentially a database which keeps track of the accounts and transactions. Moreover, we want to record the history of the transactions. Finally, we want to copy the database for backup reasons.

This problem is intended to practice shallow and deep copies of objects. The problem consists of mandatory, optional, and bonus tests. Correct implementation of the naive solution is required to pass the mandatory tests. A more advanced solution is required to pass the optional and bonus tests (and thus to earn more points). The improved solutions must conserve memory when creating object copies.

The aim is to practice and understand the difference between shallow and deep copy. Since deep copying is already handled in STL containers and in the C++ string objects, these are not available in this homework.

The required class CBank has the following interface:

default constructor
creates an empty instance - an empty database,
copy constructor
creates a deep copy of the source instance. Implement it only if the automatically generated copy constructor does not work properly (seriously, you will have to implement it),
destructor
frees the resources allocated by the instance
operator =
to deep-copy data between instances,
NewAccount(accID, initialBalance)
this method creates a new account. The parameters are:
accID is an unique identifier used to identify the account. This may be an arbitrary string,
initialBalance is the amount deposited to the account when it is created. We assume the amount is an integer, either positive, or negative (our bank allows negative balance even when the account is being created).
The method returns success(true) or error (false). An error is returned if the same accID) already exists in the database.
Transaction(debAcc, credAcc, amount, sign)
method adds a transaction (a payment). The parameters are:
debAcc identifies the payer's account,
credAcc identifiers the receiver's account,
amount is the amount being paid (may be zero or positive),
sign is a signature of the transaction. The database does not need to understand the signature, it just needs to store it with the transaction. The signature is in the form of a string.
The method returns success(true) or error (false). An error is returned if either account (debAccID or credAccID) does not exist in the database. The method does not have to check the balance of the payer's account, we assume the account may be freely overdrawn. On the other hand, the method shall cancel the transaction (and return false) if the debit and credit accounts are the same.
TrimAccount(accID)
the method releases the transactions associated with the account. In particular, the actual balance becomes the new initial balance and the list of transactions will be cleared. The method returns success(true) or error (false). An error is returned if the account does not exist in the database. Please note the method does not care about the payer's account balance.
Account(accID)
the method is used to access an account. If the account accID exists in the database, the method allows access to the account (subsequent call to output operator, or Balance, see below). If the account does not exist, the method throws an exception (any exception).
cout << x . Account( id )
this call will print the initial balance, the list of transactions and the actual balance to the stream specified. The output format is shown in the sample runs.
x . Account( id ) . Balance()
this call returns the actual balance (an integer).
Submit a source file with your CBank implementation. Moreover, add any further class (or classes) your implementation requires. The class must follow the public interface below. If there is a mismatch in the interface, the compilation will fail. You may extend the interface and add you auxiliary methods and member variables (both public and private, although private are preferred). The submitted file must include both declarations as well as implementation of the class (the methods may be implemented inline but do not have to). Do not add unnecessary classes/functions to the submitted file. In particular, if the file contains main, your tests, or any #include definitions, please, keep them in a conditional compile block. Use the attached template as a basis for your implementation. If the preprocessor definitions are preserved, the file may be submitted to Progtest.

Your implementation must not use STL (vector, list, string, ...). If you use any STL component, your program will fail to compile.

To pass the optional and bonus tests, design an efficient memory layout. The following advice may help:

the testing creates many copies where the original and the copies are not further modified. Thus, it is a good idea to share the data with the original at least until the first modification of either the original or the copied object.
The changes of the database are "minor" in that the content is modified only slightly between the copies. Thus, it makes sense to share unmodified parts between the original and the copy. For instance, if you decide to create a class representing an account, there is a few account instances modified. However, majority accounts remains the same in both the original and the copy.
The interface and sample use of the class is included in the attached archive.

A working solution of this homework may be used for code review (your solution must pass the mandatory and optional tests with 100% result; the solution does not have to pass the bonus tests).