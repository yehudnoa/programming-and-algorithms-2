#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
#endif /* __PROGTEST */
using namespace std;

#ifndef __PROGTEST__
class CTimeStamp
{
public:
    int Year;
    int Month;
    int Day;
    int Hour;
    int Min;
    int Sec;
    CTimeStamp( int year, int month, int day, int hour, int minute, int sec ){
        Year = year; Month = month; Day = day; Hour = hour; Min = minute; Sec = sec;
    };
    int Compare ( const CTimeStamp & x ) const{
        int tmp = Year - x.Year;
        if(tmp != 0) return tmp;
        tmp= Month - x.Month;
        if(tmp != 0) return tmp;
        tmp= Day - x.Day;
        if(tmp != 0) return tmp;
        tmp= Hour - x.Hour;
        if(tmp != 0) return tmp;
        tmp= Min - x.Min;
        if(tmp != 0) return tmp;
        tmp= Sec - x.Sec;
        return tmp;
    };
    friend ostream & operator << ( ostream & os, const CTimeStamp & x ){
        os << x.Year << '-' ;
        if(x.Month < 10){ os << '0' << x.Month << '-'; }else{ os << x.Month << '-'; }
        if(x.Day < 10){ os << '0' << x.Day << ' '; }else{ os << x.Day << ' '; }
        if(x.Hour < 10){ os << '0' << x.Hour << ':'; }else{ os << x.Hour << ':'; }
        if(x.Min < 10){ os << '0' << x.Min << ':'; }else{ os << x.Min << ':'; }
        if(x.Sec < 10){ os << '0' << x.Sec; }else{ os << x.Sec; }
        return os;
    };
};
//=================================================================================================
class CMailBody
{
public:
    CMailBody ( int size, const char * data ){
        m_Size = size;
        if(size==0)
            m_Data=NULL;
        else {
            m_Data = new char[size + 1];
            strcpy(m_Data, data);
        }
    }
    ~CMailBody(){
        if(m_Data!=NULL)
            delete [] m_Data;
    }
    CMailBody( const CMailBody & a ){
        m_Size = a.m_Size;
        m_Data = new char [a.m_Size+1];
        strcpy(m_Data, a.m_Data);
    }
    CMailBody &operator = (const CMailBody & a){ // operator =
        if(m_Data!=NULL)
            delete [] m_Data;
        m_Size = a.m_Size;
        m_Data = new char [a.m_Size + 1];
        strcpy(m_Data, a.m_Data);
        return *this;
    }

    // copy cons/op=/destructor is correctly implemented in the testing environment
    friend ostream & operator << ( ostream & os, const CMailBody & x )
    {
        return os << "mail body: " << x . m_Size << " B";
    }
private:
    int m_Size;
    char * m_Data;
};
//=================================================================================================
class CAttach
{
public:
    CAttach ( int x ) : m_X (x), m_RefCnt ( 1 )
    {
    }
    void AddRef ( void ) const
    {
        m_RefCnt ++;
    }
    void Release ( void ) const
    {
        if ( !--m_RefCnt )
            delete this;
    }
private:
    int m_X;
    mutable int m_RefCnt;
    CAttach ( const CAttach   & x );
    CAttach & operator = ( const CAttach   & x );
    ~CAttach ( void ) = default;
    friend ostream & operator << ( ostream & os, const CAttach & x )
    {
        return os << "attachment: " << x . m_X << " B";
    }
};
//=================================================================================================
#endif /* __PROGTEST__, DO NOT remove */
//=================================================================================================




class CMail
{
public:


    CTimeStamp timeS = CTimeStamp(0,0,0,0,0,0);
    CMailBody body = CMailBody(0, "");
    string from;
    const CAttach *attach;

    CMail ( const CTimeStamp & timeStamp, const string & frm, const CMailBody & bdy, const CAttach * attch ){
        timeS = timeStamp;
        body = bdy;
        from = frm;
        attach = attch;
        if(attach != nullptr)
            attach->AddRef();
    };
    ~CMail(){
    }
    const string     & From ( void ) const{
        return from;
    };
    const CMailBody  & Body  ( void ) const{
        return body;
    };
    const CTimeStamp & TimeStamp ( void ) const{
        return timeS;
    };
    const CAttach* Attachment ( void ) const{
        return attach;
    };

    friend ostream & operator << ( ostream & os, const CMail & x ){
        if(x.Attachment() != nullptr)
            os << x.TimeStamp() << ' ' << x.From() << ' ' << x.Body() << " + " << *x.Attachment();
        else
            os << x.TimeStamp() << ' ' << x.From() << ' ' << x.Body();
        return os;
    };
private:
    // todo
};

//=================================================================================================

struct Folder{
    vector<CMail>mail;
    string name;
};

//=================================================================================================

class CMailBox
{
public:
    vector<Folder>flder;

    CMailBox ( void ){
        Folder newflder ;
        newflder.name = "inbox";
        flder.emplace_back(newflder);
    };

    ~CMailBox(){
        for(int i = flder.size()-1; i>=0; i--){
            for(int j = flder[i].mail.size()-1; j>=0; j--){
                flder[i].mail[j].body;
            }
            flder[i].mail.clear();
        }
        flder.clear();
    }

    bool Delivery ( const CMail & mail ){
        vector<CMail>::iterator it;
        it = lower_bound(flder[0].mail.begin(), flder[0].mail.end(), mail, Comparator);
        flder[0].mail.insert(it,mail);
        return true;
    };

    bool NewFolder ( const string & folderName ){
        Folder newflder;
        newflder.name = folderName;
        if(folderName.compare(flder[0].name) == 0){
            return false;
        }
        if(binary_search(flder.begin() + 1, flder.end(), newflder, Comparator_Name)){
            return false;
        }
        vector<Folder>::iterator it;
        it = lower_bound(flder.begin()+1, flder.end(), newflder, Comparator_Name);
        flder.insert(it,newflder);
        return true;
    }

    bool MoveMail ( const string & fromFolder, const string & toFolder ){
        Folder tmp, tmp1, *From, *To;
        tmp.name = fromFolder;
        tmp1.name = toFolder;
        if(fromFolder.compare(toFolder) == 0){
            return true;
        }
        if(fromFolder.compare(flder[0].name) == 0){
            From = &flder[0];
        }else if( binary_search(flder.begin()+1, flder.end(), tmp, Comparator_Name)){
            vector<Folder>::iterator it;
            it = lower_bound(flder.begin()+1, flder.end(), tmp, Comparator_Name);
            From = &flder[it - flder.begin()];
        }else{
            return false;
        }

        if(toFolder.compare(flder[0].name) == 0){
            To = &flder[0];
        }
        else if(binary_search(flder.begin()+1, flder.end(), tmp1, Comparator_Name)){
            vector<Folder>::iterator it1;
            it1 = lower_bound(flder.begin()+1, flder.end(), tmp1, Comparator_Name);
            To = &flder[it1 - flder.begin()];
        }else{
            return false;
        }
        auto it = std::next(From->mail.begin(), From->mail.size());
        move(From->mail.begin(), it, back_inserter(To->mail));
        From->mail.clear();
        sort(To->mail.begin(),To->mail.end(),Comparator);
        return true;

    };

    list<CMail> ListMail ( const string & folderName, const CTimeStamp & from, const CTimeStamp & to ){
        int a;
        Folder tmp;
        tmp.name = folderName;
        list<CMail> M;
        if(binary_search(flder.begin() + 1, flder.end(), tmp, Comparator_Name) || folderName.compare(flder[0].name)==0){
            vector<Folder>::iterator it;
            if(folderName.compare(flder[0].name)==0){
                a=0;
            }else{
                it = lower_bound(flder.begin()+1, flder.end(), tmp, Comparator_Name);
                a = it-flder.begin();
            }
            string t;
            CMail tmp(from, "0", CMailBody(1, "mail content 1"), nullptr);
            vector<CMail>::iterator begin, end;
            begin = lower_bound(flder[a].mail.begin(), flder[a].mail.end(), tmp, Comparator);
            tmp.timeS = to;
            end = upper_bound(flder[a].mail.begin(), flder[a].mail.end(), tmp, Comparator);
            copy(begin, end, back_inserter(M));
        }
        return M;
    };

    set<string> ListAddr ( const CTimeStamp & from, const CTimeStamp & to ) {
        set<string> S;
        vector<CMail>::iterator begin, end;
        CMail tmp(from, "0", CMailBody(1, "mail content 1"), nullptr);
        for(int a = 0; a < (int)flder.size(); a++){
            tmp.timeS = from;
            begin = lower_bound(flder[a].mail.begin(), flder[a].mail.end(), tmp, Comparator);
            tmp.timeS = to;
            end = upper_bound(flder[a].mail.begin(), flder[a].mail.end(), tmp, Comparator);
            int b = begin - flder[a].mail.begin();
            int c = end - flder[a].mail.begin();
            for(; b < c; b++){
                S.emplace(flder[a].mail[b].from);
            }
        }
        return S;
    };
private:
    static bool  Comparator(const CMail & a, const CMail & b)
    {
        return a.TimeStamp().Compare(b.TimeStamp()) < 0;
    }
    static bool  Comparator_Time(const CTimeStamp & a, const CMail & b)
    {
        return a.Compare(b.TimeStamp()) < 0;
    }
    static bool  Comparator_Name(const Folder & a, const Folder & b)
    {
        return a.name.compare(b.name) < 0;
    }
    int search(string tmp){
        for(int a = 0; a < (int)flder.size(); a++){
            if(tmp.compare(flder[a].name) == 0){
                return a;
            }
        }
        return -1;
    }
};


//=================================================================================================
#ifndef __PROGTEST__
static string showMail ( const list<CMail> & l )
{
    ostringstream oss;
    for ( const auto & x : l ) {
        cout << x << endl;
        oss << x << endl;
    }
    return oss . str ();
}
static string showUsers ( const set<string> & s )
{
    ostringstream oss;
    for ( const auto & x : s ){
        cout << x << endl;
        oss << x << endl;
    }
    return oss . str ();
}
int main ( void )
{
    list<CMail> mailList;
    set<string> users;
    CAttach   * att;

    CMailBox m0;
    assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 24, 13 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 1" ), nullptr ) ) );
    m0.NewFolder("aa");
    assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 26, 23 ), "user2@fit.cvut.cz", CMailBody ( 22, "some different content" ), nullptr ) ) );
    att = new CAttach ( 200 );
    assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 11, 23, 43 ), "boss1@fit.cvut.cz", CMailBody ( 14, "urgent message" ), att ) ) );
    assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 18, 52, 27 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 2" ), att ) ) );
    att -> Release ();
    att = new CAttach ( 97 );
    assert ( m0 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 16, 12, 48 ), "boss1@fit.cvut.cz", CMailBody ( 24, "even more urgent message" ), att ) ) );
    att -> Release ();
    assert ( showMail ( m0 . ListMail ( "inbox",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B + attachment: 200 B\n"
                                                                                       "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B\n"
                                                                                       "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B + attachment: 97 B\n"
                                                                                       "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 200 B\n" );
    assert ( showMail ( m0 . ListMail ( "inbox",
                                        CTimeStamp ( 2014, 3, 31, 15, 26, 23 ),
                                        CTimeStamp ( 2014, 3, 31, 16, 12, 48 ) ) ) == "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B\n"
                                                                                      "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B + attachment: 97 B\n" );
    assert ( showUsers ( m0 . ListAddr ( CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                         CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "boss1@fit.cvut.cz\n"
                                                                                        "user1@fit.cvut.cz\n"
                                                                                        "user2@fit.cvut.cz\n" );
    assert ( showUsers ( m0 . ListAddr ( CTimeStamp ( 2014, 3, 31, 15, 26, 23 ),
                                         CTimeStamp ( 2014, 3, 31, 16, 12, 48 ) ) ) == "boss1@fit.cvut.cz\n"
                                                                                       "user2@fit.cvut.cz\n" );

    CMailBox m1;
    assert ( m1 . NewFolder ( "work" ) );
    assert ( m1 . NewFolder ( "spam" ) );
    assert ( !m1 . NewFolder ( "spam" ) );
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 24, 13 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 1" ), nullptr ) ) );
    att = new CAttach ( 500 );
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 15, 26, 23 ), "user2@fit.cvut.cz", CMailBody ( 22, "some different content" ), att ) ) );
    att -> Release ();
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 11, 23, 43 ), "boss1@fit.cvut.cz", CMailBody ( 14, "urgent message" ), nullptr ) ) );
    att = new CAttach ( 468 );
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 18, 52, 27 ), "user1@fit.cvut.cz", CMailBody ( 14, "mail content 2" ), att ) ) );
    att -> Release ();
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 16, 12, 48 ), "boss1@fit.cvut.cz", CMailBody ( 24, "even more urgent message" ), nullptr ) ) );
    assert ( showMail ( m1 . ListMail ( "inbox",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                                                                                       "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                                                                                       "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n" );
    assert ( showMail ( m1 . ListMail ( "work",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "" );
    assert ( m1 . MoveMail ( "inbox", "work" ) );
    assert ( showMail ( m1 . ListMail ( "inbox",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "" );
    assert ( showMail ( m1 . ListMail ( "work",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                                                                                       "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                                                                                       "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n" );
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 19, 24, 13 ), "user2@fit.cvut.cz", CMailBody ( 14, "mail content 4" ), nullptr ) ) );
    att = new CAttach ( 234 );
    assert ( m1 . Delivery ( CMail ( CTimeStamp ( 2014, 3, 31, 13, 26, 23 ), "user3@fit.cvut.cz", CMailBody ( 9, "complains" ), att ) ) );
    att -> Release ();
    assert ( showMail ( m1 . ListMail ( "inbox",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 13:26:23 user3@fit.cvut.cz mail body: 9 B + attachment: 234 B\n"
                                                                                       "2014-03-31 19:24:13 user2@fit.cvut.cz mail body: 14 B\n" );
    assert ( showMail ( m1 . ListMail ( "work",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                                                                                       "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                                                                                       "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n" );
    assert ( m1 . MoveMail ( "inbox", "work" ) );
    assert ( showMail ( m1 . ListMail ( "inbox",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "" );
    assert ( showMail ( m1 . ListMail ( "work",
                                        CTimeStamp ( 2000, 1, 1, 0, 0, 0 ),
                                        CTimeStamp ( 2050, 12, 31, 23, 59, 59 ) ) ) == "2014-03-31 11:23:43 boss1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 13:26:23 user3@fit.cvut.cz mail body: 9 B + attachment: 234 B\n"
                                                                                       "2014-03-31 15:24:13 user1@fit.cvut.cz mail body: 14 B\n"
                                                                                       "2014-03-31 15:26:23 user2@fit.cvut.cz mail body: 22 B + attachment: 500 B\n"
                                                                                       "2014-03-31 16:12:48 boss1@fit.cvut.cz mail body: 24 B\n"
                                                                                       "2014-03-31 18:52:27 user1@fit.cvut.cz mail body: 14 B + attachment: 468 B\n"
                                                                                       "2014-03-31 19:24:13 user2@fit.cvut.cz mail body: 14 B\n" );

    return 0;
}
#endif /* __PROGTEST__ */
