#ifndef __PROGTEST__
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

vector<int> width;
vector<int> hight;
///parent class
class CType{
public:
    //int m_hight, m_width, place;
    CType() = default;
    virtual void print(ostream& os, int width, int hight, int i, int j) = 0;
    virtual ~CType() = default;
    virtual CType *clone() const = 0;
    virtual int size_w() = 0;
    virtual int size_h() = 0;
};

///child class that takes in text
class CText: public CType
{
public:
    int m_hight = 0, m_width = 0, place = -1;
    vector<string> type;
    static const int ALIGN_LEFT=1 , ALIGN_RIGHT=0;
    //constructor
    CText(const char * str, int align){
        string line;
        istringstream iss(str);
        place = align;
        while(getline(iss , line)){
            type.emplace_back(line);
            if(m_width < (int)line.size()){
                m_width = (int)line.size();
            }
            m_hight += 1;
        }
    }
    CText(const CText & a){
        m_hight = a.m_hight;
        m_width = a.m_width;
        place = a.place;
        for(int i = 0; i < (int)a.type.size(); i++){
            type.emplace_back(a.type[i]);
        }
    }
    ~CText() override {
        type.clear();
    }
    CText *clone() const override {
        auto * tmp = new CText(*this);
        return tmp;
    }
    int size_w() override {
        return this->m_width;
    }
    int size_h() override {
        return this->m_hight;
    }
    //delets current cell then put in new text
    CText & SetText(const char * text){
        type.clear();
        m_hight = 0; m_width = 0;
        string line;
        istringstream iss(text);
        while(getline(iss , line)){
            type.emplace_back(line);
            if(m_width < (int)line.size()){
                m_width = (int)line.size();
            }
            m_hight += 1;
        }
        return *this;
    }
    void print(ostream& os, int width, int hight, int i, int j) override {
        os << '|';
        if(i > (int)type.size()-1){
            for(int a = 0; a < width; a++){
                os << " ";
            }
        }
        else{
            if(place == 1){
                os << type[i];
                for(int s = (int)type[i].length(); s < width; s++){
                    os << ' ';
                }
            }
            else if(place == 0){
                for(int s = (int)type[i].length(); s < width; s++){
                    os << ' ';
                }
                os << type[i];
            }
        }

    };
};

///child class for en empty cell
class CEmpty: public CType {
public:
    int m_hight = 0, m_width = 0, place = -1;
    CEmpty(){

    }
    CEmpty(const CEmpty & a){
        m_hight = a.m_hight;
        m_width = a.m_width;
    }
    ~CEmpty() override { }
    int size_w() override {
        return this->m_width;
    }
    int size_h() override {
        return this->m_hight;
    }
    CEmpty *clone() const override {
        auto * tmp = new CEmpty(*this);
        return tmp;
    }
    void print(ostream& os, int width, int hight, int i, int j) override {
        os << "|";
        for(int s = 0; s < width; s++){
            os << " ";
        }
    };

};

///child class for taking in assci pic
class CImage: public CType
{
public:
    int m_hight = 0, m_width = 0, place = -1;
    vector<string> type;

    //constuctor empty
    CImage(){ }
    ~CImage() override {
        type.clear();
    }
    CImage(const CImage & a){
        m_hight = a.m_hight;
        m_width = a.m_width;
        for(int i = 0; i < (int)a.type.size(); i++){
            type.emplace_back(a.type[i]);
        }
    }

    CImage *clone() const override {
        auto * tmp = new CImage(*this);
        return tmp;
    }
    int size_w() override {
        return this->m_width;
    }
    int size_h() override {
        return this->m_hight;
    }
    //adds new row to pic
    CImage & AddRow(const char * row){
        type.emplace_back(row);
        if((int)strlen(row) > m_width){
            m_width = (int)strlen(row);
        }
        m_hight += 1;
        return *this;
    }

    void print(ostream& os, int width, int hight, int i, int j) override {
        int tmp;
        int width1;
        tmp = (hight - this->m_hight);
        if((tmp % 2) == 0){
            tmp /= 2;
        }else{
            tmp -= 1;
            tmp /= 2;
        }

        os << '|';
        if(i < tmp || i > (((int)type.size()-1)+tmp)){
            for(int a = 0; a < width; a++){
                os << " ";
            }
        }
        else{
            width -= type[i-tmp].length();
            if((width % 2) == 0){
                width /= 2;
                width1 = width;
            }else{
                width /= 2;
                width1 = width+1;
            }

            for(int s = 0; s < width; s++){
                os << ' ';
            }
            os << type[i-tmp];
            for(int s = 0; s < width1; s++){
                os << ' ';
            }
        }
    };
};

///main class that holds the cells
class CTable : public CType
{
public:
    int row, column;
    CType*** cells;
    vector<string> tmp;
    vector<int> width1;
    vector<int> hight1;
    //a matrix that is the size of r*c of CType
    //constuctor gets size
    CTable(const int & r , const int & c ){
        row = r;
        column = c;
        cells = new CType ** [row];
        for(int a = 0 ; a < row ; a++){
            cells[a] = new CType* [column];
        }
        for(int j =0 ; j<row;j++){
            for(int i = 0; i < column; i++){
                cells[j][i]= new CEmpty();
            }
        }

        width1.resize(column, 0);
        hight1.resize(row, 0);
    }


    ~CTable(){
        for(int r = 0; r < row; r++){
            for(int c = 0; c < column; c++){
                delete cells[r][c];
            }
            delete [] cells[r];
        }
        delete [] cells;
    }

    CTable(const CTable & a){
        row = a.row;
        column = a.column;
        cells = new CType ** [row];
        for(int a = 0 ; a < row ; a++){
            cells[a] = new CType* [column];
        }
        for(int r = 0; r < row; r++){
            for(int c = 0; c < column; c++){
                cells[r][c] = a.cells[r][c]->clone();
            }
        }
        width1.resize(column, 0);
        hight1.resize(row, 0);
    }

    void SetCell(const int & r, const int & c, const CType & cell){
        if(r<row && c<column) {
            if (&cell != cells[r][c]) {
                CType *clone = cell.clone();
                delete cells[r][c];
                cells[r][c] = clone;
                size_check1();
            }
        }
    }

    //returns one cell
    CType & GetCell(int r, int c){
        return *cells[r][c];
    }
    friend ostringstream & operator <<(ostringstream & os, CTable a){
        a.line(os, 0);
        for(int i = 0; i < a.row; i++){
            for(int j = 0; j < hight[i]; j++){
                for(int k = 0; k < a.column; k++){

                    a.cells[i][k]->print(os, width[k], hight[i], j, i);
                }
                os << "|" << endl;

            }
            a.line(os, 1);
        }
        for(int c = 0; c < (int)hight.size(); c++){
            cout << hight[c] << endl;
        }
        cout << os.str();
        cout << endl;
        return os;
    }

    CTable &operator =(const CTable & a){
        if(&a != this){
            for(int r = 0; r < row; r++){
                for(int c = 0; c < column; c++){
                    delete cells[r][c];
                }
                delete [] cells[r];
            }
            delete [] cells;

            row = a.row;
            column = a.column;
            width1.resize(column, 0);
            hight1.resize(row, 0);
            cells = new CType ** [row];

            for(int a = 0 ; a < row ; a++){
                cells[a] = new CType* [column];
            }

            for(int r = 0; r < row; r++){
                for(int c = 0; c < column; c++){
                    cells[r][c] = a.cells[r][c]->clone();
                }
            }
        }
        return *this;
    }

    //////////////////////////////////////added part//////////////////////////////////////////
    CTable *clone() const override { // clone CTable
        return new CTable(*this);
    }
    //returns width of all table
    int size_w() override {
        size_check1();
        int w = 0;
        for(int i = 0; i < (int)width1.size(); i++){
            w += width1[i];
        }
        w += column+1;
        return w;
    }
    //returns hight of all table
    int size_h() override {
        size_check1();
        int h = 0;
        for(int i = 0; i < (int)hight1.size(); i++){
            h += hight1[i];
        }
        h += row+1;
        return h;
    }
    // puts all the table in string vector
    void create_cell(){
        ostringstream oss;
        this->line1(0);
        for(int a = 0; a < row; a++){
            for(int b = 0; b < hight1[a]; b++){
                oss << "|";
                for(int k = 0; k < column; k++){

                    cells[a][k]->print(oss, width1[k], hight1[a], b, a);
                }
                oss << "|";
                tmp.push_back(oss.str());
                oss . str ("");
                oss . clear ();
            }
            this->line1(1);
        }
    }
    //return line in vector that is needed
    void print(ostream& os, int width, int hight, int i, int j) override {

        string t;
        if(i == 0){
            create_cell();
        }
        if(i < (int)tmp.size()){
            os << tmp[i];
            int length = (int)tmp[i].length();
            for(int e = (width - length); e >= 0; e--){
                os << " ";
            }
        }else{
            os << "|";
            for(int c = 0; c < width; c++){
                os << " ";
            }
        }
    }
private:
    //for << op
    void line (ostringstream& os, int i){
        if(i == 0){
            size_check();
        }
        os << "+";
        for(int i = 0; i < (int)width.size(); i++ ){
            for(int j = 0; j < width[i]; j++){
                os << "-";
            }
            os << "+";
        }
        os << endl;
    }
    //for print function
    void line1 ( int i) {
        string t;
        t.push_back('|');
        t.push_back('+');
        for(int i = 0; i < (int)width1.size(); i++ ){
            for(int j = 0; j < width1[i]; j++){
                t.push_back('-');
            }
            t.push_back('+');
        }
        tmp.emplace_back(t);
    }
    //puts the biggest width and hight of each column and row in width1 array and hight1 array;
    void size_check() {
        width.resize(column, 0);
        hight.resize(row, 0);
        for(int c = 0; c < column; c++){
            width[c] = 0;
        }
        for(int r = 0; r < row; r++){
            hight[r] = 0;
        }
        for(int c = 0; c < column; c++){
            for(int r = 0; r < row; r++){
                width_check(c, cells[r][c]->size_w());
            }
        }
        for(int c = 0; c < row; c++){
            for(int r = 0; r < column; r++){
                hight_check(c, cells[c][r]->size_h());
            }
        }
    }
    void size_check1() {
        for(int c = 0; c < column; c++){
            width1[c] = 0;
        }
        for(int r = 0; r < row; r++){
            hight1[r] = 0;
        }
        for(int c = 0; c < row; c++){
            for(int r = 0; r < column; r++){
                hight1_check(c, cells[c][r]->size_h());
            }
        }
        for(int c = 0; c < column; c++){
            for(int r = 0; r < row; r++){
                width1_check(c, cells[r][c]->size_w());
            }
        }
    }
    void width_check(int col, int size) {
        if(width[col] < size){
            width[col] = size;
        }
    }
    void hight_check(int row, int size) {
        if(hight[row] < size){
            hight[row] = size;
        }
    }
    void width1_check(int col, int size) {
        if(width1[col] < size){
            width1[col] = size;
        }
    }
    void hight1_check(int row, int size) {
        if(hight1[row] < size){
            hight1[row] = size;
        }
    }
};
#ifndef __PROGTEST__
int main ( void )
{
        ostringstream oss;
        CTable t0 ( 3, 2 );
        t0 . SetCell ( 0, 0, CText ( "Hello,\n"
                                     "Hello Kitty", CText::ALIGN_LEFT ) );
        t0 . SetCell ( 1, 0, CText ( "Lorem ipsum dolor sit amet", CText::ALIGN_LEFT ) );
        t0 . SetCell ( 2, 0, CText ( "Bye,\n"
                                     "Hello Kitty", CText::ALIGN_RIGHT ) );
        t0 . SetCell ( 1, 1, CImage ()
                . AddRow ( "###                   " )
                . AddRow ( "#  #                  " )
                . AddRow ( "#  # # ##   ###    ###" )
                . AddRow ( "###  ##    #   #  #  #" )
                . AddRow ( "#    #     #   #  #  #" )
                . AddRow ( "#    #     #   #  #  #" )
                . AddRow ( "#    #      ###    ###" )
                . AddRow ( "                     #" )
                . AddRow ( "                   ## " )
                . AddRow ( "                      " )
                . AddRow ( " #    ###   ###   #   " )
                . AddRow ( "###  #   # #     ###  " )
                . AddRow ( " #   #####  ###   #   " )
                . AddRow ( " #   #         #  #   " )
                . AddRow ( "  ##  ###   ###    ## " ) );
        t0 . SetCell ( 2, 1, CEmpty () );
        oss . str ("");
        oss . clear ();
        oss << t0;
        assert ( oss . str () ==
                 "+--------------------------+----------------------+\n"
                 "|Hello,                    |                      |\n"
                 "|Hello Kitty               |                      |\n"
                 "+--------------------------+----------------------+\n"
                 "|Lorem ipsum dolor sit amet|###                   |\n"
                 "|                          |#  #                  |\n"
                 "|                          |#  # # ##   ###    ###|\n"
                 "|                          |###  ##    #   #  #  #|\n"
                 "|                          |#    #     #   #  #  #|\n"
                 "|                          |#    #     #   #  #  #|\n"
                 "|                          |#    #      ###    ###|\n"
                 "|                          |                     #|\n"
                 "|                          |                   ## |\n"
                 "|                          |                      |\n"
                 "|                          | #    ###   ###   #   |\n"
                 "|                          |###  #   # #     ###  |\n"
                 "|                          | #   #####  ###   #   |\n"
                 "|                          | #   #         #  #   |\n"
                 "|                          |  ##  ###   ###    ## |\n"
                 "+--------------------------+----------------------+\n"
                 "|                      Bye,|                      |\n"
                 "|               Hello Kitty|                      |\n"
                 "+--------------------------+----------------------+\n" );
        t0 . SetCell ( 0, 1, t0 . GetCell ( 1, 1 ) );
        t0 . SetCell ( 2, 1, CImage ()
                . AddRow ( "*****   *      *  *      ******* ******  *" )
                . AddRow ( "*    *  *      *  *      *            *  *" )
                . AddRow ( "*    *  *      *  *      *           *   *" )
                . AddRow ( "*    *  *      *  *      *****      *    *" )
                . AddRow ( "****    *      *  *      *         *     *" )
                . AddRow ( "*  *    *      *  *      *        *       " )
                . AddRow ( "*   *   *      *  *      *       *       *" )
                . AddRow ( "*    *    *****   ****** ******* ******  *" ) );
        dynamic_cast<CText &> ( t0 . GetCell ( 1, 0 ) ) . SetText ( "Lorem ipsum dolor sit amet,\n"
                                                                    "consectetur adipiscing\n"
                                                                    "elit. Curabitur scelerisque\n"
                                                                    "lorem vitae lectus cursus,\n"
                                                                    "vitae porta ante placerat. Class aptent taciti\n"
                                                                    "sociosqu ad litora\n"
                                                                    "torquent per\n"
                                                                    "conubia nostra,\n"
                                                                    "per inceptos himenaeos.\n"
                                                                    "\n"
                                                                    "Donec tincidunt augue\n"
                                                                    "sit amet metus\n"
                                                                    "pretium volutpat.\n"
                                                                    "Donec faucibus,\n"
                                                                    "ante sit amet\n"
                                                                    "luctus posuere,\n"
                                                                    "mauris tellus" );
        oss . str ("");
        oss . clear ();
        oss << t0;
        assert ( oss . str () ==
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Hello,                                        |          ###                             |\n"
                 "|Hello Kitty                                   |          #  #                            |\n"
                 "|                                              |          #  # # ##   ###    ###          |\n"
                 "|                                              |          ###  ##    #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #      ###    ###          |\n"
                 "|                                              |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|                                              |                                          |\n"
                 "|                                              |           #    ###   ###   #             |\n"
                 "|                                              |          ###  #   # #     ###            |\n"
                 "|                                              |           #   #####  ###   #             |\n"
                 "|                                              |           #   #         #  #             |\n"
                 "|                                              |            ##  ###   ###    ##           |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                   |                                          |\n"
                 "|consectetur adipiscing                        |          ###                             |\n"
                 "|elit. Curabitur scelerisque                   |          #  #                            |\n"
                 "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
                 "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
                 "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
                 "|torquent per                                  |          #    #     #   #  #  #          |\n"
                 "|conubia nostra,                               |          #    #      ###    ###          |\n"
                 "|per inceptos himenaeos.                       |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|Donec tincidunt augue                         |                                          |\n"
                 "|sit amet metus                                |           #    ###   ###   #             |\n"
                 "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
                 "|Donec faucibus,                               |           #   #####  ###   #             |\n"
                 "|ante sit amet                                 |           #   #         #  #             |\n"
                 "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
                 "|mauris tellus                                 |                                          |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                              |*    *  *      *  *      *           *   *|\n"
                 "|                                              |*    *  *      *  *      *****      *    *|\n"
                 "|                                              |****    *      *  *      *         *     *|\n"
                 "|                                              |*  *    *      *  *      *        *       |\n"
                 "|                                              |*   *   *      *  *      *       *       *|\n"
                 "|                                              |*    *    *****   ****** ******* ******  *|\n"
                 "+----------------------------------------------+------------------------------------------+\n" );
        CTable t1 ( t0 );
        t1 . SetCell ( 1, 0, CEmpty () );
        t1 . SetCell ( 1, 1, CEmpty () );
        oss . str ("");
        oss . clear ();
        oss << t0;
        assert ( oss . str () ==
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Hello,                                        |          ###                             |\n"
                 "|Hello Kitty                                   |          #  #                            |\n"
                 "|                                              |          #  # # ##   ###    ###          |\n"
                 "|                                              |          ###  ##    #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #      ###    ###          |\n"
                 "|                                              |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|                                              |                                          |\n"
                 "|                                              |           #    ###   ###   #             |\n"
                 "|                                              |          ###  #   # #     ###            |\n"
                 "|                                              |           #   #####  ###   #             |\n"
                 "|                                              |           #   #         #  #             |\n"
                 "|                                              |            ##  ###   ###    ##           |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                   |                                          |\n"
                 "|consectetur adipiscing                        |          ###                             |\n"
                 "|elit. Curabitur scelerisque                   |          #  #                            |\n"
                 "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
                 "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
                 "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
                 "|torquent per                                  |          #    #     #   #  #  #          |\n"
                 "|conubia nostra,                               |          #    #      ###    ###          |\n"
                 "|per inceptos himenaeos.                       |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|Donec tincidunt augue                         |                                          |\n"
                 "|sit amet metus                                |           #    ###   ###   #             |\n"
                 "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
                 "|Donec faucibus,                               |           #   #####  ###   #             |\n"
                 "|ante sit amet                                 |           #   #         #  #             |\n"
                 "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
                 "|mauris tellus                                 |                                          |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                              |*    *  *      *  *      *           *   *|\n"
                 "|                                              |*    *  *      *  *      *****      *    *|\n"
                 "|                                              |****    *      *  *      *         *     *|\n"
                 "|                                              |*  *    *      *  *      *        *       |\n"
                 "|                                              |*   *   *      *  *      *       *       *|\n"
                 "|                                              |*    *    *****   ****** ******* ******  *|\n"
                 "+----------------------------------------------+------------------------------------------+\n" );
        oss . str ("");
        oss . clear ();
        oss << t1;
        assert ( oss . str () ==
                 "+-----------+------------------------------------------+\n"
                 "|Hello,     |          ###                             |\n"
                 "|Hello Kitty|          #  #                            |\n"
                 "|           |          #  # # ##   ###    ###          |\n"
                 "|           |          ###  ##    #   #  #  #          |\n"
                 "|           |          #    #     #   #  #  #          |\n"
                 "|           |          #    #     #   #  #  #          |\n"
                 "|           |          #    #      ###    ###          |\n"
                 "|           |                               #          |\n"
                 "|           |                             ##           |\n"
                 "|           |                                          |\n"
                 "|           |           #    ###   ###   #             |\n"
                 "|           |          ###  #   # #     ###            |\n"
                 "|           |           #   #####  ###   #             |\n"
                 "|           |           #   #         #  #             |\n"
                 "|           |            ##  ###   ###    ##           |\n"
                 "+-----------+------------------------------------------+\n"
                 "+-----------+------------------------------------------+\n"
                 "|       Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|           |*    *  *      *  *      *           *   *|\n"
                 "|           |*    *  *      *  *      *****      *    *|\n"
                 "|           |****    *      *  *      *         *     *|\n"
                 "|           |*  *    *      *  *      *        *       |\n"
                 "|           |*   *   *      *  *      *       *       *|\n"
                 "|           |*    *    *****   ****** ******* ******  *|\n"
                 "+-----------+------------------------------------------+\n" );
        t1 = t0;
        t1 . SetCell ( 0, 0, CEmpty () );
        t1 . SetCell ( 1, 1, CImage ()
                . AddRow ( "  ********                    " )
                . AddRow ( " **********                   " )
                . AddRow ( "**        **                  " )
                . AddRow ( "**             **        **   " )
                . AddRow ( "**             **        **   " )
                . AddRow ( "***         ********  ********" )
                . AddRow ( "****        ********  ********" )
                . AddRow ( "****           **        **   " )
                . AddRow ( "****           **        **   " )
                . AddRow ( "****      **                  " )
                . AddRow ( " **********                   " )
                . AddRow ( "  ********                    " ) );
        oss . str ("");
        oss . clear ();
        oss << t0;
        assert ( oss . str () ==
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Hello,                                        |          ###                             |\n"
                 "|Hello Kitty                                   |          #  #                            |\n"
                 "|                                              |          #  # # ##   ###    ###          |\n"
                 "|                                              |          ###  ##    #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #      ###    ###          |\n"
                 "|                                              |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|                                              |                                          |\n"
                 "|                                              |           #    ###   ###   #             |\n"
                 "|                                              |          ###  #   # #     ###            |\n"
                 "|                                              |           #   #####  ###   #             |\n"
                 "|                                              |           #   #         #  #             |\n"
                 "|                                              |            ##  ###   ###    ##           |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                   |                                          |\n"
                 "|consectetur adipiscing                        |          ###                             |\n"
                 "|elit. Curabitur scelerisque                   |          #  #                            |\n"
                 "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
                 "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
                 "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
                 "|torquent per                                  |          #    #     #   #  #  #          |\n"
                 "|conubia nostra,                               |          #    #      ###    ###          |\n"
                 "|per inceptos himenaeos.                       |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|Donec tincidunt augue                         |                                          |\n"
                 "|sit amet metus                                |           #    ###   ###   #             |\n"
                 "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
                 "|Donec faucibus,                               |           #   #####  ###   #             |\n"
                 "|ante sit amet                                 |           #   #         #  #             |\n"
                 "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
                 "|mauris tellus                                 |                                          |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                              |*    *  *      *  *      *           *   *|\n"
                 "|                                              |*    *  *      *  *      *****      *    *|\n"
                 "|                                              |****    *      *  *      *         *     *|\n"
                 "|                                              |*  *    *      *  *      *        *       |\n"
                 "|                                              |*   *   *      *  *      *       *       *|\n"
                 "|                                              |*    *    *****   ****** ******* ******  *|\n"
                 "+----------------------------------------------+------------------------------------------+\n" );
        oss . str ("");
        oss . clear ();
        oss << t1;
        assert ( oss . str () ==
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|                                              |          ###                             |\n"
                 "|                                              |          #  #                            |\n"
                 "|                                              |          #  # # ##   ###    ###          |\n"
                 "|                                              |          ###  ##    #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #      ###    ###          |\n"
                 "|                                              |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|                                              |                                          |\n"
                 "|                                              |           #    ###   ###   #             |\n"
                 "|                                              |          ###  #   # #     ###            |\n"
                 "|                                              |           #   #####  ###   #             |\n"
                 "|                                              |           #   #         #  #             |\n"
                 "|                                              |            ##  ###   ###    ##           |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                   |                                          |\n"
                 "|consectetur adipiscing                        |                                          |\n"
                 "|elit. Curabitur scelerisque                   |        ********                          |\n"
                 "|lorem vitae lectus cursus,                    |       **********                         |\n"
                 "|vitae porta ante placerat. Class aptent taciti|      **        **                        |\n"
                 "|sociosqu ad litora                            |      **             **        **         |\n"
                 "|torquent per                                  |      **             **        **         |\n"
                 "|conubia nostra,                               |      ***         ********  ********      |\n"
                 "|per inceptos himenaeos.                       |      ****        ********  ********      |\n"
                 "|                                              |      ****           **        **         |\n"
                 "|Donec tincidunt augue                         |      ****           **        **         |\n"
                 "|sit amet metus                                |      ****      **                        |\n"
                 "|pretium volutpat.                             |       **********                         |\n"
                 "|Donec faucibus,                               |        ********                          |\n"
                 "|ante sit amet                                 |                                          |\n"
                 "|luctus posuere,                               |                                          |\n"
                 "|mauris tellus                                 |                                          |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                              |*    *  *      *  *      *           *   *|\n"
                 "|                                              |*    *  *      *  *      *****      *    *|\n"
                 "|                                              |****    *      *  *      *         *     *|\n"
                 "|                                              |*  *    *      *  *      *        *       |\n"
                 "|                                              |*   *   *      *  *      *       *       *|\n"
                 "|                                              |*    *    *****   ****** ******* ******  *|\n"
                 "+----------------------------------------------+------------------------------------------+\n" );
        CTable t2 ( 2, 2 );
        t2 . SetCell ( 0, 0, CText ( "OOP", CText::ALIGN_LEFT ) );
        t2 . SetCell ( 0, 1, CText ( "Encapsulation", CText::ALIGN_LEFT ) );
        t2 . SetCell ( 1, 0, CText ( "Polymorphism", CText::ALIGN_LEFT ) );
        t2 . SetCell ( 1, 1, CText ( "Inheritance", CText::ALIGN_LEFT ) );
        oss . str ("");
        oss . clear ();
        oss << t2;
        assert ( oss . str () ==
                 "+------------+-------------+\n"
                 "|OOP         |Encapsulation|\n"
                 "+------------+-------------+\n"
                 "|Polymorphism|Inheritance  |\n"
                 "+------------+-------------+\n" );
        t1 . SetCell ( 0, 0, t2 );
        dynamic_cast<CText &> ( t2 . GetCell ( 0, 0 ) ) . SetText ( "Object Oriented Programming" );
        oss . str ("");
        oss . clear ();
        oss << t2;
        assert ( oss . str () ==
                 "+---------------------------+-------------+\n"
                 "|Object Oriented Programming|Encapsulation|\n"
                 "+---------------------------+-------------+\n"
                 "|Polymorphism               |Inheritance  |\n"
                 "+---------------------------+-------------+\n" );
        oss . str ("");
        oss . clear ();
        oss << t1;
        assert ( oss . str () ==
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|+------------+-------------+                  |          ###                             |\n"
                 "||OOP         |Encapsulation|                  |          #  #                            |\n"
                 "|+------------+-------------+                  |          #  # # ##   ###    ###          |\n"
                 "||Polymorphism|Inheritance  |                  |          ###  ##    #   #  #  #          |\n"
                 "|+------------+-------------+                  |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #     #   #  #  #          |\n"
                 "|                                              |          #    #      ###    ###          |\n"
                 "|                                              |                               #          |\n"
                 "|                                              |                             ##           |\n"
                 "|                                              |                                          |\n"
                 "|                                              |           #    ###   ###   #             |\n"
                 "|                                              |          ###  #   # #     ###            |\n"
                 "|                                              |           #   #####  ###   #             |\n"
                 "|                                              |           #   #         #  #             |\n"
                 "|                                              |            ##  ###   ###    ##           |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                   |                                          |\n"
                 "|consectetur adipiscing                        |                                          |\n"
                 "|elit. Curabitur scelerisque                   |        ********                          |\n"
                 "|lorem vitae lectus cursus,                    |       **********                         |\n"
                 "|vitae porta ante placerat. Class aptent taciti|      **        **                        |\n"
                 "|sociosqu ad litora                            |      **             **        **         |\n"
                 "|torquent per                                  |      **             **        **         |\n"
                 "|conubia nostra,                               |      ***         ********  ********      |\n"
                 "|per inceptos himenaeos.                       |      ****        ********  ********      |\n"
                 "|                                              |      ****           **        **         |\n"
                 "|Donec tincidunt augue                         |      ****           **        **         |\n"
                 "|sit amet metus                                |      ****      **                        |\n"
                 "|pretium volutpat.                             |       **********                         |\n"
                 "|Donec faucibus,                               |        ********                          |\n"
                 "|ante sit amet                                 |                                          |\n"
                 "|luctus posuere,                               |                                          |\n"
                 "|mauris tellus                                 |                                          |\n"
                 "+----------------------------------------------+------------------------------------------+\n"
                 "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                              |*    *  *      *  *      *           *   *|\n"
                 "|                                              |*    *  *      *  *      *****      *    *|\n"
                 "|                                              |****    *      *  *      *         *     *|\n"
                 "|                                              |*  *    *      *  *      *        *       |\n"
                 "|                                              |*   *   *      *  *      *       *       *|\n"
                 "|                                              |*    *    *****   ****** ******* ******  *|\n"
                 "+----------------------------------------------+------------------------------------------+\n" );
        t1 . SetCell ( 0, 0, t1 );
        oss . str ("");
        oss . clear ();
        oss << t1;
        assert ( oss . str () ==
                 "+-------------------------------------------------------------------------------------------+------------------------------------------+\n"
                 "|+----------------------------------------------+------------------------------------------+|                                          |\n"
                 "||+------------+-------------+                  |          ###                             ||                                          |\n"
                 "|||OOP         |Encapsulation|                  |          #  #                            ||                                          |\n"
                 "||+------------+-------------+                  |          #  # # ##   ###    ###          ||                                          |\n"
                 "|||Polymorphism|Inheritance  |                  |          ###  ##    #   #  #  #          ||                                          |\n"
                 "||+------------+-------------+                  |          #    #     #   #  #  #          ||                                          |\n"
                 "||                                              |          #    #     #   #  #  #          ||                                          |\n"
                 "||                                              |          #    #      ###    ###          ||                                          |\n"
                 "||                                              |                               #          ||                                          |\n"
                 "||                                              |                             ##           ||                                          |\n"
                 "||                                              |                                          ||                                          |\n"
                 "||                                              |           #    ###   ###   #             ||                                          |\n"
                 "||                                              |          ###  #   # #     ###            ||                                          |\n"
                 "||                                              |           #   #####  ###   #             ||                                          |\n"
                 "||                                              |           #   #         #  #             ||          ###                             |\n"
                 "||                                              |            ##  ###   ###    ##           ||          #  #                            |\n"
                 "|+----------------------------------------------+------------------------------------------+|          #  # # ##   ###    ###          |\n"
                 "||Lorem ipsum dolor sit amet,                   |                                          ||          ###  ##    #   #  #  #          |\n"
                 "||consectetur adipiscing                        |                                          ||          #    #     #   #  #  #          |\n"
                 "||elit. Curabitur scelerisque                   |        ********                          ||          #    #     #   #  #  #          |\n"
                 "||lorem vitae lectus cursus,                    |       **********                         ||          #    #      ###    ###          |\n"
                 "||vitae porta ante placerat. Class aptent taciti|      **        **                        ||                               #          |\n"
                 "||sociosqu ad litora                            |      **             **        **         ||                             ##           |\n"
                 "||torquent per                                  |      **             **        **         ||                                          |\n"
                 "||conubia nostra,                               |      ***         ********  ********      ||           #    ###   ###   #             |\n"
                 "||per inceptos himenaeos.                       |      ****        ********  ********      ||          ###  #   # #     ###            |\n"
                 "||                                              |      ****           **        **         ||           #   #####  ###   #             |\n"
                 "||Donec tincidunt augue                         |      ****           **        **         ||           #   #         #  #             |\n"
                 "||sit amet metus                                |      ****      **                        ||            ##  ###   ###    ##           |\n"
                 "||pretium volutpat.                             |       **********                         ||                                          |\n"
                 "||Donec faucibus,                               |        ********                          ||                                          |\n"
                 "||ante sit amet                                 |                                          ||                                          |\n"
                 "||luctus posuere,                               |                                          ||                                          |\n"
                 "||mauris tellus                                 |                                          ||                                          |\n"
                 "|+----------------------------------------------+------------------------------------------+|                                          |\n"
                 "||                                          Bye,|*****   *      *  *      ******* ******  *||                                          |\n"
                 "||                                   Hello Kitty|*    *  *      *  *      *            *  *||                                          |\n"
                 "||                                              |*    *  *      *  *      *           *   *||                                          |\n"
                 "||                                              |*    *  *      *  *      *****      *    *||                                          |\n"
                 "||                                              |****    *      *  *      *         *     *||                                          |\n"
                 "||                                              |*  *    *      *  *      *        *       ||                                          |\n"
                 "||                                              |*   *   *      *  *      *       *       *||                                          |\n"
                 "||                                              |*    *    *****   ****** ******* ******  *||                                          |\n"
                 "|+----------------------------------------------+------------------------------------------+|                                          |\n"
                 "+-------------------------------------------------------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                                                                |                                          |\n"
                 "|consectetur adipiscing                                                                     |                                          |\n"
                 "|elit. Curabitur scelerisque                                                                |        ********                          |\n"
                 "|lorem vitae lectus cursus,                                                                 |       **********                         |\n"
                 "|vitae porta ante placerat. Class aptent taciti                                             |      **        **                        |\n"
                 "|sociosqu ad litora                                                                         |      **             **        **         |\n"
                 "|torquent per                                                                               |      **             **        **         |\n"
                 "|conubia nostra,                                                                            |      ***         ********  ********      |\n"
                 "|per inceptos himenaeos.                                                                    |      ****        ********  ********      |\n"
                 "|                                                                                           |      ****           **        **         |\n"
                 "|Donec tincidunt augue                                                                      |      ****           **        **         |\n"
                 "|sit amet metus                                                                             |      ****      **                        |\n"
                 "|pretium volutpat.                                                                          |       **********                         |\n"
                 "|Donec faucibus,                                                                            |        ********                          |\n"
                 "|ante sit amet                                                                              |                                          |\n"
                 "|luctus posuere,                                                                            |                                          |\n"
                 "|mauris tellus                                                                              |                                          |\n"
                 "+-------------------------------------------------------------------------------------------+------------------------------------------+\n"
                 "|                                                                                       Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                                                                Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                                                                           |*    *  *      *  *      *           *   *|\n"
                 "|                                                                                           |*    *  *      *  *      *****      *    *|\n"
                 "|                                                                                           |****    *      *  *      *         *     *|\n"
                 "|                                                                                           |*  *    *      *  *      *        *       |\n"
                 "|                                                                                           |*   *   *      *  *      *       *       *|\n"
                 "|                                                                                           |*    *    *****   ****** ******* ******  *|\n"
                 "+-------------------------------------------------------------------------------------------+------------------------------------------+\n" );
        t1 . SetCell ( 0, 0, t1 );
        oss . str ("");
        oss . clear ();
        oss << t1;
        assert ( oss . str () ==
                 "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n"
                 "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
                 "||+----------------------------------------------+------------------------------------------+|                                          ||                                          |\n"
                 "|||+------------+-------------+                  |          ###                             ||                                          ||                                          |\n"
                 "||||OOP         |Encapsulation|                  |          #  #                            ||                                          ||                                          |\n"
                 "|||+------------+-------------+                  |          #  # # ##   ###    ###          ||                                          ||                                          |\n"
                 "||||Polymorphism|Inheritance  |                  |          ###  ##    #   #  #  #          ||                                          ||                                          |\n"
                 "|||+------------+-------------+                  |          #    #     #   #  #  #          ||                                          ||                                          |\n"
                 "|||                                              |          #    #     #   #  #  #          ||                                          ||                                          |\n"
                 "|||                                              |          #    #      ###    ###          ||                                          ||                                          |\n"
                 "|||                                              |                               #          ||                                          ||                                          |\n"
                 "|||                                              |                             ##           ||                                          ||                                          |\n"
                 "|||                                              |                                          ||                                          ||                                          |\n"
                 "|||                                              |           #    ###   ###   #             ||                                          ||                                          |\n"
                 "|||                                              |          ###  #   # #     ###            ||                                          ||                                          |\n"
                 "|||                                              |           #   #####  ###   #             ||                                          ||                                          |\n"
                 "|||                                              |           #   #         #  #             ||          ###                             ||                                          |\n"
                 "|||                                              |            ##  ###   ###    ##           ||          #  #                            ||                                          |\n"
                 "||+----------------------------------------------+------------------------------------------+|          #  # # ##   ###    ###          ||                                          |\n"
                 "|||Lorem ipsum dolor sit amet,                   |                                          ||          ###  ##    #   #  #  #          ||                                          |\n"
                 "|||consectetur adipiscing                        |                                          ||          #    #     #   #  #  #          ||                                          |\n"
                 "|||elit. Curabitur scelerisque                   |        ********                          ||          #    #     #   #  #  #          ||                                          |\n"
                 "|||lorem vitae lectus cursus,                    |       **********                         ||          #    #      ###    ###          ||                                          |\n"
                 "|||vitae porta ante placerat. Class aptent taciti|      **        **                        ||                               #          ||                                          |\n"
                 "|||sociosqu ad litora                            |      **             **        **         ||                             ##           ||                                          |\n"
                 "|||torquent per                                  |      **             **        **         ||                                          ||                                          |\n"
                 "|||conubia nostra,                               |      ***         ********  ********      ||           #    ###   ###   #             ||                                          |\n"
                 "|||per inceptos himenaeos.                       |      ****        ********  ********      ||          ###  #   # #     ###            ||                                          |\n"
                 "|||                                              |      ****           **        **         ||           #   #####  ###   #             ||                                          |\n"
                 "|||Donec tincidunt augue                         |      ****           **        **         ||           #   #         #  #             ||                                          |\n"
                 "|||sit amet metus                                |      ****      **                        ||            ##  ###   ###    ##           ||          ###                             |\n"
                 "|||pretium volutpat.                             |       **********                         ||                                          ||          #  #                            |\n"
                 "|||Donec faucibus,                               |        ********                          ||                                          ||          #  # # ##   ###    ###          |\n"
                 "|||ante sit amet                                 |                                          ||                                          ||          ###  ##    #   #  #  #          |\n"
                 "|||luctus posuere,                               |                                          ||                                          ||          #    #     #   #  #  #          |\n"
                 "|||mauris tellus                                 |                                          ||                                          ||          #    #     #   #  #  #          |\n"
                 "||+----------------------------------------------+------------------------------------------+|                                          ||          #    #      ###    ###          |\n"
                 "|||                                          Bye,|*****   *      *  *      ******* ******  *||                                          ||                               #          |\n"
                 "|||                                   Hello Kitty|*    *  *      *  *      *            *  *||                                          ||                             ##           |\n"
                 "|||                                              |*    *  *      *  *      *           *   *||                                          ||                                          |\n"
                 "|||                                              |*    *  *      *  *      *****      *    *||                                          ||           #    ###   ###   #             |\n"
                 "|||                                              |****    *      *  *      *         *     *||                                          ||          ###  #   # #     ###            |\n"
                 "|||                                              |*  *    *      *  *      *        *       ||                                          ||           #   #####  ###   #             |\n"
                 "|||                                              |*   *   *      *  *      *       *       *||                                          ||           #   #         #  #             |\n"
                 "|||                                              |*    *    *****   ****** ******* ******  *||                                          ||            ##  ###   ###    ##           |\n"
                 "||+----------------------------------------------+------------------------------------------+|                                          ||                                          |\n"
                 "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
                 "||Lorem ipsum dolor sit amet,                                                                |                                          ||                                          |\n"
                 "||consectetur adipiscing                                                                     |                                          ||                                          |\n"
                 "||elit. Curabitur scelerisque                                                                |        ********                          ||                                          |\n"
                 "||lorem vitae lectus cursus,                                                                 |       **********                         ||                                          |\n"
                 "||vitae porta ante placerat. Class aptent taciti                                             |      **        **                        ||                                          |\n"
                 "||sociosqu ad litora                                                                         |      **             **        **         ||                                          |\n"
                 "||torquent per                                                                               |      **             **        **         ||                                          |\n"
                 "||conubia nostra,                                                                            |      ***         ********  ********      ||                                          |\n"
                 "||per inceptos himenaeos.                                                                    |      ****        ********  ********      ||                                          |\n"
                 "||                                                                                           |      ****           **        **         ||                                          |\n"
                 "||Donec tincidunt augue                                                                      |      ****           **        **         ||                                          |\n"
                 "||sit amet metus                                                                             |      ****      **                        ||                                          |\n"
                 "||pretium volutpat.                                                                          |       **********                         ||                                          |\n"
                 "||Donec faucibus,                                                                            |        ********                          ||                                          |\n"
                 "||ante sit amet                                                                              |                                          ||                                          |\n"
                 "||luctus posuere,                                                                            |                                          ||                                          |\n"
                 "||mauris tellus                                                                              |                                          ||                                          |\n"
                 "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
                 "||                                                                                       Bye,|*****   *      *  *      ******* ******  *||                                          |\n"
                 "||                                                                                Hello Kitty|*    *  *      *  *      *            *  *||                                          |\n"
                 "||                                                                                           |*    *  *      *  *      *           *   *||                                          |\n"
                 "||                                                                                           |*    *  *      *  *      *****      *    *||                                          |\n"
                 "||                                                                                           |****    *      *  *      *         *     *||                                          |\n"
                 "||                                                                                           |*  *    *      *  *      *        *       ||                                          |\n"
                 "||                                                                                           |*   *   *      *  *      *       *       *||                                          |\n"
                 "||                                                                                           |*    *    *****   ****** ******* ******  *||                                          |\n"
                 "|+-------------------------------------------------------------------------------------------+------------------------------------------+|                                          |\n"
                 "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n"
                 "|Lorem ipsum dolor sit amet,                                                                                                             |                                          |\n"
                 "|consectetur adipiscing                                                                                                                  |                                          |\n"
                 "|elit. Curabitur scelerisque                                                                                                             |        ********                          |\n"
                 "|lorem vitae lectus cursus,                                                                                                              |       **********                         |\n"
                 "|vitae porta ante placerat. Class aptent taciti                                                                                          |      **        **                        |\n"
                 "|sociosqu ad litora                                                                                                                      |      **             **        **         |\n"
                 "|torquent per                                                                                                                            |      **             **        **         |\n"
                 "|conubia nostra,                                                                                                                         |      ***         ********  ********      |\n"
                 "|per inceptos himenaeos.                                                                                                                 |      ****        ********  ********      |\n"
                 "|                                                                                                                                        |      ****           **        **         |\n"
                 "|Donec tincidunt augue                                                                                                                   |      ****           **        **         |\n"
                 "|sit amet metus                                                                                                                          |      ****      **                        |\n"
                 "|pretium volutpat.                                                                                                                       |       **********                         |\n"
                 "|Donec faucibus,                                                                                                                         |        ********                          |\n"
                 "|ante sit amet                                                                                                                           |                                          |\n"
                 "|luctus posuere,                                                                                                                         |                                          |\n"
                 "|mauris tellus                                                                                                                           |                                          |\n"
                 "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n"
                 "|                                                                                                                                    Bye,|*****   *      *  *      ******* ******  *|\n"
                 "|                                                                                                                             Hello Kitty|*    *  *      *  *      *            *  *|\n"
                 "|                                                                                                                                        |*    *  *      *  *      *           *   *|\n"
                 "|                                                                                                                                        |*    *  *      *  *      *****      *    *|\n"
                 "|                                                                                                                                        |****    *      *  *      *         *     *|\n"
                 "|                                                                                                                                        |*  *    *      *  *      *        *       |\n"
                 "|                                                                                                                                        |*   *   *      *  *      *       *       *|\n"
                 "|                                                                                                                                        |*    *    *****   ****** ******* ******  *|\n"
                 "+----------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------+\n" );

        return 0;
    }
#endif /* __PROGTEST__ */