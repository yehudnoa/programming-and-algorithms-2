#ifndef __PROGTEST__
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

vector<int> width;
vector<int> hight;

///parent class
class CType{
public:
    //int m_hight, m_width, place;
    CType() = default;
    virtual void print(ostream& os, int width, int i, int j) const = 0;
    virtual ~CType() = default;
    virtual CType *clone() const = 0;
    virtual int size_w() = 0;
    virtual int size_h() = 0;
};

///child class that takes in text
class CText: public CType
{
public:
    int m_hight = 0, m_width = 0, place = -1;
    vector<string> type;
    static const int ALIGN_LEFT=1 , ALIGN_RIGHT=0;
    //constructor
    CText(const char * str, int align){
        string line;
        istringstream iss(str);
        place = align;
        while(getline(iss , line)){
            type.emplace_back(line);
            if(m_width < (int)line.size()){
                m_width = (int)line.size();
            }
            m_hight += 1;
        }
    }
    CText(const CText & a){
        m_hight = a.m_hight;
        m_width = a.m_width;
        place = a.place;
        for(int i = 0; i < (int)a.type.size(); i++){
            type.emplace_back(a.type[i]);
        }
    }
    ~CText() override {
        type.clear();
    }
    CText *clone() const override {
        auto * tmp = new CText(*this);
        return tmp;
    }
    int size_w() override {
        return this->m_width;
    }
    int size_h() override {
        return this->m_hight;
    }
    //delets current cell then put in new text
    CText & SetText(const char * text){
        type.clear();
        m_hight = 0; m_width = 0;
        string line;
        istringstream iss(text);
        while(getline(iss , line)){
            type.emplace_back(line);
            if(m_width < (int)line.size()){
                m_width = (int)line.size();
            }
            m_hight += 1;
        }
        return *this;
    }
    void print(ostream& os, int width, int i, int j) const override {
        os << '|';
        if(i > (int)type.size()-1){
            for(int a = 0; a < width; a++){
                os << " ";
            }
        }
        else{
            if(place == 1){
                os << type[i];
                for(int s = (int)type[i].length(); s < width; s++){
                    os << ' ';
                }
            }
            else if(place == 0){
                for(int s = (int)type[i].length(); s < width; s++){
                    os << ' ';
                }
                os << type[i];
            }
        }

    };
};

///child class for en empty cell
class CEmpty: public CType {
public:
    int m_hight = 0, m_width = 0, place = -1;
    CEmpty(){

    }
    CEmpty(const CEmpty & a){
        m_hight = a.m_hight;
        m_width = a.m_width;
    }
    ~CEmpty() override { }
    int size_w() override {
        return this->m_width;
    }
    int size_h() override {
        return this->m_hight;
    }
    CEmpty *clone() const override {
        auto * tmp = new CEmpty(*this);
        return tmp;
    }
    void print(ostream& os, int width, int i, int j) const override {
        os << "|";
        for(int s = 0; s < width; s++){
            os << " ";
        }
    };

};

///child class for taking in assci pic
class CImage: public CType
{
public:
    int m_hight = 0, m_width = 0, place = -1;
    vector<string> type;
    //constuctor empty
    CImage(){ }
    ~CImage() override {
        type.clear();
    }
    CImage(const CImage & a){
        m_hight = a.m_hight;
        m_width = a.m_width;
        for(int i = 0; i < (int)a.type.size(); i++){
            type.emplace_back(a.type[i]);
        }
    }
    CImage *clone() const override {
        auto * tmp = new CImage(*this);
        return tmp;
    }
    int size_w() override {
        return this->m_width;
    }
    int size_h() override {
        return this->m_hight;
    }
    //adds new row to pic
    CImage & AddRow(const char * row){
        type.emplace_back(row);
        if((int)strlen(row) > m_width){
            m_width = (int)strlen(row);
        }
        m_hight += 1;
        return *this;
    }

    void print(ostream& os, int width, int i, int j) const override {
        int tmp;
        int width1;
        tmp = (hight[j] - this->m_hight);
        if((tmp % 2) == 0){
            tmp /= 2;
        }else{
            tmp -= 1;
            tmp /= 2;
        }

        os << '|';
        if(i < tmp || i > (((int)type.size()-1)+tmp)){
            for(int a = 0; a < width; a++){
                os << " ";
            }
        }
        else{
            width -= type[i-tmp].length();
            if((width % 2) == 0){
                width /= 2;
                width1 = width;
            }else{
                width /= 2;
                width1 = width+1;
            }

            for(int s = 0; s < width; s++){
                os << ' ';
            }
            os << type[i-tmp];
            for(int s = 0; s < width1; s++){
                os << ' ';
            }
        }
    };
};

///main class that holds the cells
class CTable
{
public:
    int row, column;
    CType*** cells;

    //a matrix that is the size of r*c of CType
    //constuctor gets size
    CTable(int r , int c ){
        row = r;
        column = c;
        cells = new CType ** [row];
        for(int a = 0 ; a < row ; a++){
            cells[a] = new CType* [column];
        }
        for(int j =0 ; j<row;j++)
            for(int i = 0; i < column; i++)
                cells[j][i]= new CEmpty();
        width.resize(column, 0);
        hight.resize(row, 0);
    }
    ~CTable(){
        for(int r = 0; r < row; r++){
            for(int c = 0; c < column; c++){
                delete cells[r][c];
            }
            delete [] cells[r];
        }
        delete [] cells;
    }

    CTable(CTable & a){
        row = a.row;
        column = a.column;
        cells = new CType ** [row];
        for(int a = 0 ; a < row ; a++){
            cells[a] = new CType* [column];
        }
        for(int r = 0; r < row; r++){
            for(int c = 0; c < column; c++){
                cells[r][c] = a.cells[r][c]->clone();
            }
        }
    }
    void SetCell(const int & r, const int & c, const CType & cell){
        if(r<row && c<column) {
            if (&cell != cells[r][c]) {
                CType *clone = cell.clone();
                delete cells[r][c];
                cells[r][c] = clone;
            }
        }
    }

    //returns one cell
    CType & GetCell(int r, int c){
        return *cells[r][c];
    }
    friend ostringstream & operator <<(ostringstream & os, CTable a){
        a.line(os, 0);
        for(int i = 0; i < a.row; i++){
            for(int j = 0; j < hight[i]; j++){
                for(int k = 0; k < a.column; k++){
                    a.cells[i][k]->print(os, width[k], j, i);
                }
                os << "|" << endl;
            }
            a.line(os, 1);
        }
        //cout << os.str();
        cout << endl;
        return os;
    }
    CTable & operator =(const CTable a){
        if(&a != this){
            for(int r = 0; r < row; r++){
                for(int c = 0; c < column; c++){
                    delete cells[r][c];
                }
                delete [] cells[r];
            }
            delete [] cells;

            row = a.row;
            column = a.column;
            width.resize(column, 0);
            hight.resize(row, 0);
            cells = new CType ** [row];

            for(int a = 0 ; a < row ; a++){
                cells[a] = new CType* [column];
            }

            for(int r = 0; r < row; r++){
                for(int c = 0; c < column; c++){
                    cells[r][c] = a.cells[r][c]->clone();
                }
            }
        }
        return *this;
    }
private:
    void line (ostringstream& os, int i){
        if(i == 0){
            size_check();
        }
        os << "+";
        cout << "+";
        for(int i = 0; i < (int)width.size(); i++ ){
            for(int j = 0; j < width[i]; j++){
                os << "-";
                cout << "-";
            }
            os << "+";
            cout << "+";
        }
        os << endl;
        cout << endl;
    }
    void size_check(){
        for(int i = 0; i < column; i++){
            width[i] = 0;
        }
        for(int i = 0; i < row; i++){
            hight[i] = 0;
        }
        for(int c = 0; c < column; c++){
            for(int r = 0; r < row; r++){
                width_check(c, cells[r][c]->size_w());
            }
        }
        for(int c = 0; c < row; c++){
            for(int r = 0; r < column; r++){
                hight_check(c, cells[c][r]->size_h());
            }
        }
    }
    void width_check(int col, int size){
        if(width[col] < size){
            width[col] = size;
        }
    }
    void hight_check(int row, int size){
        if(hight[row] < size){
            hight[row] = size;
        }
    }
};
#ifndef __PROGTEST__
int main ( void )
{
    ostringstream oss;
    CTable t0 ( 3, 2 );
    t0 . SetCell ( 0, 0, CText ( "Hello,\n"
                                 "Hello Kitty", CText::ALIGN_LEFT ) );
    t0 . SetCell ( 1, 0, CText ( "Lorem ipsum dolor sit amet", CText::ALIGN_LEFT ) );
    t0 . SetCell ( 2, 0, CText ( "Bye,\n"
                                 "Hello Kitty", CText::ALIGN_RIGHT ) );
    t0 . SetCell ( 1, 1, CImage ()
            . AddRow ( "###                   " )
            . AddRow ( "#  #                  " )
            . AddRow ( "#  # # ##   ###    ###" )
            . AddRow ( "###  ##    #   #  #  #" )
            . AddRow ( "#    #     #   #  #  #" )
            . AddRow ( "#    #     #   #  #  #" )
            . AddRow ( "#    #      ###    ###" )
            . AddRow ( "                     #" )
            . AddRow ( "                   ## " )
            . AddRow ( "                      " )
            . AddRow ( " #    ###   ###   #   " )
            . AddRow ( "###  #   # #     ###  " )
            . AddRow ( " #   #####  ###   #   " )
            . AddRow ( " #   #         #  #   " )
            . AddRow ( "  ##  ###   ###    ## " ) );
    t0 . SetCell ( 2, 1, CEmpty () );
    /*oss . str ("");
    oss . clear ();
    oss << t0;
    assert ( oss . str () ==
             "+--------------------------+----------------------+\n"
             "|Hello,                    |                      |\n"
             "|Hello Kitty               |                      |\n"
             "+--------------------------+----------------------+\n"
             "|Lorem ipsum dolor sit amet|###                   |\n"
             "|                          |#  #                  |\n"
             "|                          |#  # # ##   ###    ###|\n"
             "|                          |###  ##    #   #  #  #|\n"
             "|                          |#    #     #   #  #  #|\n"
             "|                          |#    #     #   #  #  #|\n"
             "|                          |#    #      ###    ###|\n"
             "|                          |                     #|\n"
             "|                          |                   ## |\n"
             "|                          |                      |\n"
             "|                          | #    ###   ###   #   |\n"
             "|                          |###  #   # #     ###  |\n"
             "|                          | #   #####  ###   #   |\n"
             "|                          | #   #         #  #   |\n"
             "|                          |  ##  ###   ###    ## |\n"
             "+--------------------------+----------------------+\n"
             "|                      Bye,|                      |\n"
             "|               Hello Kitty|                      |\n"
             "+--------------------------+----------------------+\n" );
    t0 . SetCell ( 0, 1, t0 . GetCell ( 1, 1 ) );
    t0 . SetCell ( 2, 1, CImage ()
            . AddRow ( "*****   *      *  *      ******* ******  *" )
            . AddRow ( "*    *  *      *  *      *            *  *" )
            . AddRow ( "*    *  *      *  *      *           *   *" )
            . AddRow ( "*    *  *      *  *      *****      *    *" )
            . AddRow ( "****    *      *  *      *         *     *" )
            . AddRow ( "*  *    *      *  *      *        *       " )
            . AddRow ( "*   *   *      *  *      *       *       *" )
            . AddRow ( "*    *    *****   ****** ******* ******  *" ) );*/
    dynamic_cast<CText &> ( t0 . GetCell ( 1, 0 ) ) . SetText ( "Lorem ipsum dolor sit amet,\n"
                                                                "consectetur adipiscing\n"
                                                                "elit. Curabitur scelerisque\n"
                                                                "lorem vitae lectus cursus,\n"
                                                                "vitae porta ante placerat. Class aptent taciti\n"
                                                                "sociosqu ad litora\n"
                                                                "torquent per\n"
                                                                "conubia nostra,\n"
                                                                "per inceptos himenaeos.\n"
                                                                "\n"
                                                                "Donec tincidunt augue\n"
                                                                "sit amet metus\n"
                                                                "pretium volutpat.\n"
                                                                "Donec faucibus,\n"
                                                                "ante sit amet\n"
                                                                "luctus posuere,\n"
                                                                "mauris tellus" );
    /*oss . str ("");
    oss . clear ();
    oss << t0;
    assert ( oss . str () ==
             "+----------------------------------------------+------------------------------------------+\n"
             "|Hello,                                        |          ###                             |\n"
             "|Hello Kitty                                   |          #  #                            |\n"
             "|                                              |          #  # # ##   ###    ###          |\n"
             "|                                              |          ###  ##    #   #  #  #          |\n"
             "|                                              |          #    #     #   #  #  #          |\n"
             "|                                              |          #    #     #   #  #  #          |\n"
             "|                                              |          #    #      ###    ###          |\n"
             "|                                              |                               #          |\n"
             "|                                              |                             ##           |\n"
             "|                                              |                                          |\n"
             "|                                              |           #    ###   ###   #             |\n"
             "|                                              |          ###  #   # #     ###            |\n"
             "|                                              |           #   #####  ###   #             |\n"
             "|                                              |           #   #         #  #             |\n"
             "|                                              |            ##  ###   ###    ##           |\n"
             "+----------------------------------------------+------------------------------------------+\n"
             "|Lorem ipsum dolor sit amet,                   |                                          |\n"
             "|consectetur adipiscing                        |          ###                             |\n"
             "|elit. Curabitur scelerisque                   |          #  #                            |\n"
             "|lorem vitae lectus cursus,                    |          #  # # ##   ###    ###          |\n"
             "|vitae porta ante placerat. Class aptent taciti|          ###  ##    #   #  #  #          |\n"
             "|sociosqu ad litora                            |          #    #     #   #  #  #          |\n"
             "|torquent per                                  |          #    #     #   #  #  #          |\n"
             "|conubia nostra,                               |          #    #      ###    ###          |\n"
             "|per inceptos himenaeos.                       |                               #          |\n"
             "|                                              |                             ##           |\n"
             "|Donec tincidunt augue                         |                                          |\n"
             "|sit amet metus                                |           #    ###   ###   #             |\n"
             "|pretium volutpat.                             |          ###  #   # #     ###            |\n"
             "|Donec faucibus,                               |           #   #####  ###   #             |\n"
             "|ante sit amet                                 |           #   #         #  #             |\n"
             "|luctus posuere,                               |            ##  ###   ###    ##           |\n"
             "|mauris tellus                                 |                                          |\n"
             "+----------------------------------------------+------------------------------------------+\n"
             "|                                          Bye,|*****   *      *  *      ******* ******  *|\n"
             "|                                   Hello Kitty|*    *  *      *  *      *            *  *|\n"
             "|                                              |*    *  *      *  *      *           *   *|\n"
             "|                                              |*    *  *      *  *      *****      *    *|\n"
             "|                                              |****    *      *  *      *         *     *|\n"
             "|                                              |*  *    *      *  *      *        *       |\n"
             "|                                              |*   *   *      *  *      *       *       *|\n"
             "|                                              |*    *    *****   ****** ******* ******  *|\n"
             "+----------------------------------------------+------------------------------------------+\n" ); */
    CTable t1 (2, 6);
    t1 . SetCell ( 1, 0, CText ( "Lorem ipsum dolor sit amet,\n"
                                 "consectetur adipiscing\n"
                                 "elit. Curabitur scelerisque\n"
                                 "lorem vitae lectus cursus,\n"
                                 "vitae porta ante placerat. Class aptent taciti\n"
                                 "sociosqu ad litora\n"
                                 "torquent per\n"
                                 "conubia nostra,\n"
                                 "per inceptos himenaeos.\n"
                                 "\n"
                                 "Donec tincidunt augue\n"
                                 "sit amet metus\n"
                                 "pretium volutpat.\n"
                                 "Donec faucibus,\n"
                                 "ante sit amet\n"
                                 "luctus posuere,\n"
                                 "mauris tellus", CText::ALIGN_RIGHT ) );
    t1 . SetCell ( 0, 0, CText ( "Lorem ipsum dolor sit amet,\n"
                                 "consectetur adipiscing\n"
                                 "elit. Curabitur scelerisque\n"
                                 "lorem vitae lectus cursus,\n"
                                 "vitae porta ante placerat. Class aptent taciti\n"
                                 "sociosqu ad litora\n"
                                 "torquent per\n"
                                 "conubia nostra,\n"
                                 "per inceptos himenaeos.\n"
                                 "\n"
                                 "Donec tincidunt augue\n"
                                 "sit amet metus\n"
                                 "pretium volutpat.\n"
                                 "Donec faucibus,\n"
                                 "ante sit amet\n"
                                 "luctus posuere,\n"
                                 "mauris tellus", CText::ALIGN_RIGHT ) );
    t1 . SetCell ( 1, 5, CText ( "Lorem ipsum dolor sit amet,\n"
                                 "consectetur adipiscing\n"
                                 "elit. Curabitur scelerisque\n"
                                 "lorem vitae lectus cursus,\n"
                                 "vitae porta ante placerat. Class aptent taciti\n"
                                 "sociosqu ad litora\n"
                                 "torquent per\n"
                                 "conubia nostra,\n"
                                 "per inceptos himenaeos.\n"
                                 "\n"
                                 "Donec tincidunt augue\n"
                                 "sit amet metus\n"
                                 "pretium volutpat.\n"
                                 "Donec faucibus,\n"
                                 "ante sit amet\n"
                                 "luctus posuere,\n"
                                 "mauris tellus", CText::ALIGN_RIGHT ) );

    t0 = t1;
    oss . str ("");
    oss . clear ();
    oss << t0;
    CTable t2 (1, 1);
    t2 = t0;
    oss . str ("");
    oss . clear ();
    oss << t2;



    return 0;
}
#endif /* __PROGTEST__ */